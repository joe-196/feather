package dev.maximilian.feather

public data class UserRequestInfo(
    val user: User,
    val miniSession: Boolean
)

public fun UserRequestInfo?.requireValid(
    action: String,
    oneOfPermission: Set<Permission>,
    allowMiniSession: Boolean = false
): UserRequestInfo {
    if (this == null) throw PermissionException(null, action)
    user.requirePermission(action, oneOfPermission)
    if (miniSession && !allowMiniSession) throw MiniSessionException(user, action)

    return this
}
