/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

public data class Group(
    val id: String,
    val name: String,
    val description: String?,
    val dnParent: String? = null,
    val userMembers: Set<String>,
    val groupMembers: Set<String>,
    val parentGroups: Set<String>,
    val owners: Set<String>,
    val ownerGroups: Set<String>,
    val ownedGroups: Set<String>
) {
    /* LDAP inheritance: a user inherits access from the groups that it is member of
     * and those groups inherit access from the their parent group */

    // TODO: implement cyclic membership protection for all these methods
    public fun hasOwnerRights(userId: String, credentialProvider: ICredentialProvider): Boolean =
        owners.contains(userId) ||
                /* ownership propagation from the dn parent group,
                 * but no ownership propagation from the membership-parent groups */
            (
                dnParent?.let {
                    credentialProvider.getGroup(it)
                }?.hasOwnerRights(userId, credentialProvider) ?: false
                ) ||
            ownerGroups.any {
                val group = credentialProvider.getGroup(it)
                group?.isUserMemberRecursive(userId, credentialProvider) ?: false
            }

    public fun isParentGroup(groupId: String, credentialProvider: ICredentialProvider): Boolean =
        parentGroups.contains(groupId) ||
            parentGroups.any {
                val group = credentialProvider.getGroup(it)
                group?.isParentGroup(groupId, credentialProvider) ?: false
            }

    public fun isOwnerGroup(groupId: String, credentialProvider: ICredentialProvider): Boolean =
        ownerGroups.contains(groupId) ||
            ownerGroups.any {
                val group = credentialProvider.getGroup(it)
                group?.isOwnerGroup(groupId, credentialProvider) ?: false
            }

    public fun isGroupMemberRecursive(groupId: String, credentialProvider: ICredentialProvider, depthCount: Int = 0): Boolean =
        groupMembers.contains(groupId) ||
            groupMembers.any {
                val group = credentialProvider.getGroup(it)
                if (depthCount> 15)
                    throw Exception("Cycling dependency in groupMembers by $groupId!")
                group?.isGroupMemberRecursive(groupId, credentialProvider, depthCount + 1) ?: false
            }

    public fun isOwnedGroupRecursive(groupId: String, credentialProvider: ICredentialProvider, depthCount: Int = 0): Boolean =
        ownedGroups.contains(groupId) ||
            ownedGroups.any {
                val group = credentialProvider.getGroup(it)
                if (depthCount> 15)
                    throw Exception("Cycling dependency in ownedGroups by $groupId!")
                group?.isOwnedGroupRecursive(groupId, credentialProvider, depthCount + 1) ?: false
            }

    public fun isUserMemberRecursive(userId: String, credentialProvider: ICredentialProvider, depthCount: Int = 0): Boolean =
        userMembers.contains(userId) ||
            // implicit membership via the child groups
            groupMembers.any {
                val group = credentialProvider.getGroup(it)
                if (depthCount> 15)
                    throw Exception("Cycling dependency in userMember by $userId!")
                group?.isUserMemberRecursive(userId, credentialProvider, depthCount + 1) ?: false
            }
}
