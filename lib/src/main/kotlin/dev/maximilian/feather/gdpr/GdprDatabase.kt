package dev.maximilian.feather.gdpr

import dev.maximilian.feather.User
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant

internal class GdprDatabase(private val db: Database) {
    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(GdprDocuments)
            SchemaUtils.createMissingTablesAndColumns(GdprUserAcceptance)
        }
    }

    private object GdprDocuments : IntIdTable("gdpr_documents") {
        val creationDate = timestamp("creation_date")
        val forceDate = timestamp("force_date")
        val validDate = timestamp("valid_date")
        val content = text("content")
    }

    private object GdprUserAcceptance : Table("gdpr_user_acceptance") {
        val userId = varchar("user_id", 50)
        val acceptanceDate = timestamp("acceptance_date")
        val documentId = reference("document_id", GdprDocuments)
        override val primaryKey: PrimaryKey
            get() = PrimaryKey(userId, documentId)
    }

    fun createDocument(document: GdprDocument): GdprDocument = transaction(db) {
        val now = Instant.now()

        require(now < document.forceDate) { "The force date needs to be in the future" }
        require(document.forceDate < document.validDate) { "The force date needs to be before the valid date" }

        val id = GdprDocuments.insertAndGetId {
            it[this.creationDate] = now
            it[this.forceDate] = document.forceDate
            it[this.validDate] = document.validDate
            it[this.content] = document.content
        }

        document.copy(id = id.value, creationDate = now)
    }

    fun getAllDocuments(): List<GdprDocument> = transaction(db) {
        GdprDocuments.selectAll().orderBy(GdprDocuments.id, SortOrder.DESC).map { it.toDocument() }
    }

    fun getAcceptanceStatusOf(document: GdprDocument): List<GdprAcceptance> = transaction(db) {
        GdprUserAcceptance
            .select { GdprUserAcceptance.documentId eq document.id }
            .map {
                GdprAcceptance(
                    userId = it[GdprUserAcceptance.userId],
                    timestamp = it[GdprUserAcceptance.acceptanceDate],
                    document = document
                )
            }
    }

    fun getAcceptanceStatusOf(document: GdprDocument, user: User): GdprAcceptance? = transaction(db) {
        GdprUserAcceptance
            .select { (GdprUserAcceptance.userId eq user.id) and (GdprUserAcceptance.documentId eq document.id) }
            .singleOrNull()
            ?.let {
                GdprAcceptance(userId = user.id, timestamp = it[GdprUserAcceptance.acceptanceDate], document = document)
            }
    }

    fun storeAcceptanceInfo(document: GdprDocument, user: User): GdprAcceptance = transaction(db) {
        getAcceptanceStatusOf(document, user) ?: run {
            require(
                GdprDocuments.slice(GdprDocuments.id).select { GdprDocuments.id eq document.id }
                    .count() == 1L
            ) {
                "Gdpr document with id ${document.id} does not exist"
            }

            val now = Instant.now()

            GdprUserAcceptance.insert {
                it[userId] = user.id
                it[documentId] = document.id
                it[acceptanceDate] = now
            }

            GdprAcceptance(userId = user.id, timestamp = now, document = document)
        }
    }

    fun deleteUserMapping(user: User) = transaction(db) {
        GdprUserAcceptance.deleteWhere { GdprUserAcceptance.userId eq user.id }
    }

    fun getLatestDocumentId(): Int? = transaction(db) {
        GdprDocuments.slice(GdprDocuments.id).selectAll().orderBy(GdprDocuments.id, SortOrder.DESC).limit(1).singleOrNull()?.let { it[GdprDocuments.id] }?.value
    }

    fun getDocument(id: Int): GdprDocument? = transaction(db) {
        GdprDocuments.select { GdprDocuments.id eq id }.singleOrNull()?.toDocument()
    }

    private fun ResultRow.toDocument() = GdprDocument(
        this[GdprDocuments.id].value,
        this[GdprDocuments.creationDate],
        this[GdprDocuments.forceDate],
        this[GdprDocuments.validDate],
        this[GdprDocuments.content]
    )
}
