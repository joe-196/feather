/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

dependencies {
    // JSON (De-)Serializer
    implementation("com.fasterxml.jackson.core", "jackson-databind", jacksonVersion)
    implementation("com.fasterxml.jackson.module", "jackson-module-kotlin", jacksonVersion)
    implementation("com.fasterxml.jackson.datatype", "jackson-datatype-jsr310", jacksonVersion)

    // Rest Client
    implementation("com.konghq", "unirest-java", unirestVersion)
    implementation("com.konghq", "unirest-objectmapper-jackson", unirestVersion)

    // Webdav driver and xml runtime
    implementation("com.github.lookfirst", "sardine", "5.9")
    implementation("org.glassfish.jaxb", "jaxb-runtime", "2.3.1")
    implementation("javax.xml.bind", "jaxb-api", "2.3.1")
}

kotlin {
    explicitApi()
}
