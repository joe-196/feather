/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs

public interface IShareAPISettingsAPI {

    // default expire date
    public fun getDefaultExpireDate(): Boolean

    public fun setDefaultExpireDate(
        defaultExpireDate: Boolean
    ): Boolean

    // default internal expire date
    public fun getDefaultInternalExpireDate(): Boolean

    public fun setDefaultInternalExpireDate(
        defaultExpireDate: Boolean
    ): Boolean

    // expire after n days
    public fun getExpireAfterNDays(): Number

    public fun setExpireAfterNDays(
        expireAfterNDays: Number
    ): Boolean

    // internal expire after n days
    public fun getInternalExpireAfterNDays(): Number

    public fun setInternalExpireAfterNDays(
        expireAfterNDays: Number
    ): Boolean

    // enforce expire date
    public fun getEnforceExpireDate(): Boolean

    public fun setEnforceExpireDate(
        enforceExpireDate: Boolean
    ): Boolean

    // enforce internal expire date
    public fun getEnforceInternalExpireDate(): Boolean

    public fun setEnforceInternalExpireDate(
        enforceExpireDate: Boolean
    ): Boolean
}
