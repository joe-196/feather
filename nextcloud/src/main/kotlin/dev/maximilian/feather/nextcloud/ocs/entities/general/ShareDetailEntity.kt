/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.ocs.entities.general

import dev.maximilian.feather.nextcloud.ocs.general.ShareType

internal data class ShareDetailEntityRaw(
    val id: Int,
    val share_type: Int,
    val uid_owner: String,
    val displayname_file_owner: String,
    val displayname_owner: String,
    val permissions: Int,
    val can_edit: Boolean,
    val can_delete: Boolean,
    val token: String?,
    val url: String?,
    val expiration: String?,
    val path: String,
    val item_type: String
)

public data class ShareDetailEntity(
    val id: Int,
    val share_type: ShareType,
    val uid_owner: String,
    val displayname_file_owner: String,
    val displayname_owner: String,
    val permissions: Int,
    val can_edit: Boolean,
    val can_delete: Boolean,
    val token: String?,
    val url: String?,
    val expiration: String?,
    val path: String,
    val item_type: String
) {
    internal companion object {
        fun fromRaw(value: ShareDetailEntityRaw) = ShareDetailEntity(
            value.id,
            ShareType.findByValue(value.share_type)!!, value.uid_owner,
            value.displayname_file_owner, value.displayname_owner,
            value.permissions, value.can_edit, value.can_delete,
            value.token, value.url, value.expiration, value.path,
            value.item_type
        )
    }
}
