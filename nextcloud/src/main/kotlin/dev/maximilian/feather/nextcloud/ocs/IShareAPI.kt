/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.nextcloud.ocs.entities.CreateShareResponseSubEntity
import dev.maximilian.feather.nextcloud.ocs.entities.general.ShareDetailEntity
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType

public data class NextcloudShare(
    public val path: String,
    public val type: ShareType,
    public val userOrGroupIDtoShareWith: String,
    public val publicUpload: Boolean,
    public val permissions: PermissionType,
    public val expireDate: String
)

public interface IShareAPI {
    public fun createShare(
        path: String,
        type: ShareType,
        userOrGroupIDtoShareWith: String,
        publicUpload: Boolean,
        permissions: PermissionType,
        expireDate: String? = null
    ): CreateShareResponseSubEntity

    public fun createShare(
        newShare: NextcloudShare
    ): CreateShareResponseSubEntity {
        return createShare(
            newShare.path,
            newShare.type,
            newShare.userOrGroupIDtoShareWith,
            newShare.publicUpload,
            newShare.permissions,
            newShare.expireDate
        )
    }

    public fun updateShare(
        shareID: Int,
        publicUpload: Boolean,
        permissions: PermissionType,
        expireDate: String? = null
    ): ShareDetailEntity

    public fun deleteShare(shareID: Int)

    public fun getSpecificShareEntity(shareID: Int): ShareDetailEntity

    public fun getAllShares(): List<ShareDetailEntity>
}
