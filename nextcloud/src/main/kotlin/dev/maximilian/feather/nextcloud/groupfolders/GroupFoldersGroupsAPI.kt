/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.groupfolders

import dev.maximilian.feather.asObject
import dev.maximilian.feather.nextcloud.ocs.entities.general.ocs
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.MetaEntity
import kong.unirest.UnirestInstance

internal data class AddGroupRequest(
    val group: String
)

internal data class AddGroupResponseEntity(
    val meta: MetaEntity,
    val data: SuccessResponse
)

internal data class ChangePermissionsRequest(
    val permissions: Int
)

internal data class ChangePermissionsResponseEntity(
    val meta: MetaEntity,
    val data: SuccessResponse
)

internal class GroupFoldersGroupsAPI(
    baseURL: String,
    private val groupFolderId: Int,
    private val restAPI: UnirestInstance
) : IGroupFoldersGroupsAPI {
    private val basePath = "apps/groupfolders"
    private val groupsPath = "$baseURL/$basePath/folders/$groupFolderId/groups"

    override fun getGroupFolderId(): Int = groupFolderId

    override fun addGroup(group: String): Boolean {
        val addGroupRequest = AddGroupRequest(group)
        val reply = restAPI.post(groupsPath)
            .header("Content-Type", "application/json")
            .body(addGroupRequest)
            .asObject<ocs<AddGroupResponseEntity>>()

        reply.throwOnErrorForGroupFolders("create")
            ?: throw IllegalStateException("::create Response Body is null")

        return reply.body.ocs.data.success
    }

    // permissions are a bitmask of dev.maximilian.feather.nextcloud.ocs.general.PermissionType
    override fun changePermissions(group: String, newPermissions: Int): Boolean {
        val changePermissionsRequest = ChangePermissionsRequest(newPermissions)
        val reply = restAPI.post("$groupsPath/$group")
            .header("Content-Type", "application/json")
            .body(changePermissionsRequest)
            .asObject<ocs<ChangePermissionsResponseEntity>>()

        reply.throwOnErrorForGroupFolders("changePermissions")
            ?: throw IllegalStateException("::changePermissions Response Body is null")

        return reply.body.ocs.data.success
    }

    override fun deleteGroup(group: String) {
        val reply = restAPI.delete("$groupsPath/$group")
            .asEmpty()

        reply.throwOnErrorForGroupFolders("delete")
    }
}
