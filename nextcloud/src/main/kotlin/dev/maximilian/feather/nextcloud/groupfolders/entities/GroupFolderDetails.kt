/*
 *
 *  *    Copyright [2021] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.groupfolders.entities

public data class GroupFolderManageDetails(
    val type: String,
    val id: String,
    val displayname: String
)

public data class GroupFolderDetails(
    val id: Int,
    val mount_point: String,
    // empty array "[]" or a map of groups with their permissions
    val groups: Map<String, Int>?,
    val quota: String,
    val size: Long, // needs 64 bit integer
    val acl: Boolean,
    val manage: List<GroupFolderManageDetails>?
)
