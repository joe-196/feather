/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.webdavAPI

import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.DirectoryEntry
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.IWebdav
import java.net.URI
import java.net.URLEncoder

public class WebdavAPI(
    private val webdavURL: String,
    private val webdav: IWebdav
) {
    public fun listFolders(path: String, withAcl: Boolean = false): List<String> {
        val encodedPath = encodePath(path)
        val url: URI = URI.create(webdavURL + encodedPath)
        val resources: List<DirectoryEntry> = webdav.list(url.toString(), withAcl)
        val directoryNames = mutableListOf<String>()

        resources.forEachIndexed { index, directoryEntry ->
            if (directoryEntry.Directory) {
                if (index != 0 || directoryEntry.Name != path.substringAfterLast("/")) {
                    directoryNames.add(directoryEntry.Name)
                }
            }
        }
        return directoryNames
    }

    public fun listFiles(path: String, withAcl: Boolean = false): List<String> {
        val encodedPath = encodePath(path)
        val url: URI = URI.create(webdavURL + encodedPath)
        val resources: List<DirectoryEntry> = webdav.list(url.toString(), withAcl)
        val entries = mutableListOf<String>()

        resources.forEachIndexed { index, directoryEntry ->
            if (index != 0 || !directoryEntry.Directory || directoryEntry.Name != path.substringAfterLast("/")) {
                entries.add(directoryEntry.Name)
            }
        }
        return entries
    }

    public fun createFolder(path: String) {
        val encodedPath = encodePath(path)
        val url: URI = URI.create(webdavURL + encodedPath)
        webdav.createDirectory(url.toString())
    }

    public fun copy(source: String, destination: String) {
        val encodedPath = encodePath(source)
        val urlSource: URI = URI.create(webdavURL + encodedPath)
        val encodedDest = encodePath(destination)
        val urlDest: URI = URI.create(webdavURL + encodedDest)
        webdav.copy(urlSource.toString(), urlDest.toString())
    }

    public fun delete(path: String) {
        val encodedPath = encodePath(path)
        val url: URI = URI.create(webdavURL + encodedPath)
        webdav.delete(url.toString())
    }

    public fun createFile(path: String, content: String) {
        val encodedPath = encodePath(path)
        val url: URI = URI.create(webdavURL + encodedPath)
        webdav.put(url.toString(), content.byteInputStream())
    }

    private fun encodePath(path: String): String =
        path.split("/").joinToString("/") { URLEncoder.encode(it, "utf-8").replace("+", "%20") }
}
