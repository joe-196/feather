/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.NextcloudUserDataEntity

public data class NextcloudUser(
    public val displayName: String,
    public val email: String,
    public val userid: String,
    public val password: String,
    public val groups: List<String>,
    public val subadmin: List<String>,
    public val quota: String,
    public val language: String
)

public interface IUserAPI {

    public fun createUser(
        userid: String,
        password: String?, // can be empty to send invitation mail
        displayName: String,
        email: String?, // must not be empty if password is empty
        groups: List<String>,
        subadmin: List<String>,
        quota: String,
        language: String
    ): Boolean

    public fun createUser(
        newUser: NextcloudUser
    ): Boolean = createUser(
        newUser.userid, newUser.password, newUser.displayName,
        newUser.email, newUser.groups, newUser.subadmin, newUser.quota, newUser.language
    )

    public fun getUserInfo(id: String): NextcloudUserDataEntity

    public fun getAllUsers(): List<String>

    public fun changeMailAddress(
        userid: String,
        newMailAddress: String
    )

    public fun deleteUser(userID: String)

    public fun enableUser(userID: String)

    public fun disableUser(userID: String)
}
