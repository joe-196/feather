/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud

import dev.maximilian.feather.nextcloud.ocs.GroupAPI
import org.junit.jupiter.api.Test

class GroupAPITest {
    companion object {
        private val groupAPI = GroupAPI(TestUtil.baseUrl, TestUtil.rest)
    }

    @Test
    fun `Test getAllGroups`() {
        val groupList = groupAPI.getAllGroups()
        checkNotNull(groupList)
        check(groupList.contains("admin"))
    }

    @Test
    fun `Test addGroup`() {
        val ok = groupAPI.addGroup("testgroup")
        checkNotNull(ok)
        check(ok)

        val groupList2 = groupAPI.getAllGroups()
        checkNotNull(groupList2)
        check(groupList2.contains("admin"))
        check(groupList2.contains("testgroup"))
    }
}
