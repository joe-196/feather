#!/bin/sh

#
#    Copyright [2021] Feather development team, see AUTHORS.md
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

SCRIPT_DIR="$(dirname "$0")"
source "$SCRIPT_DIR/variables.sh"

SQL=$(cat "$SCRIPT_DIR/sql/migrate_db.sql")
IOG_SQL=$(cat "$SCRIPT_DIR/sql/migrate_db_iog.sql")

db_ready() {
  pg_isready -q -h "$OPENPROJECT_POSTGRES_HOST" -p "$OPENPROJECT_POSTGRES_PORT" || false
  return $?
}

set +e
RESULT=1
WAITED=0
while [ $RESULT -ne 0 ]; do
  sleep 5
  db_ready
  RESULT=$?
  WAITED=$((WAITED+5))
  echo "Waited $WAITED seconds on Postgres to be ready"
done;
set -e
echo "Postgres ready, migrating data"
echo "$SQL" | PGPASSWORD=$OPENPROJECT_POSTGRES_PASSWORD psql -h "$OPENPROJECT_POSTGRES_HOST" -p "$OPENPROJECT_POSTGRES_PORT" -U "$OPENPROJECT_POSTGRES_USER" "$OPENPROJECT_POSTGRES_DATABASE"

if [ $OPENPROJECT_MIGRATE_IOG -eq 1 ]; then
  echo "Postgres ready, migrating iog data"
  echo "$IOG_SQL" | PGPASSWORD=$OPENPROJECT_POSTGRES_PASSWORD psql -h "$OPENPROJECT_POSTGRES_HOST" -p "$OPENPROJECT_POSTGRES_PORT" -U "$OPENPROJECT_POSTGRES_USER" "$OPENPROJECT_POSTGRES_DATABASE"
fi

echo "Postgres migrated"
