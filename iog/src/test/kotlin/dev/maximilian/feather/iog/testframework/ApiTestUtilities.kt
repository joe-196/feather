/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.AppBuilder
import dev.maximilian.feather.Group
import dev.maximilian.feather.RedisFactory
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.CredentialAuthorizer
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.authorization.api.LoginRequest
import dev.maximilian.feather.authorization.api.SessionAnswer
import dev.maximilian.feather.iog.IogPlugin
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminInterestedPattern
import dev.maximilian.feather.iog.internal.groupPattern.SimpleMemberAdminPattern
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.settings.OPPName
import dev.maximilian.feather.iog.mockserver.GroupSyncMock
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogConfig
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.IControllableService
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.nextcloud.NextcloudSettings
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.openproject.IOpenProject
import io.javalin.Javalin
import kong.unirest.Config
import kong.unirest.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool
import java.sql.DriverManager
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal fun getEnv(name: String, default: String? = null): String =
    System.getenv(name) ?: default ?: throw IllegalArgumentException("Environment variable $name not found")

class ApiTestUtilities {
    companion object {
        val db = Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:feather;DB_CLOSE_DELAY=-1") })
        private const val basepathPrefix = "http://127.0.0.1"
        private const val basepathSuffix = "/v1"

        internal val pool: JedisPool by lazy {
            val testSetting = mutableMapOf<String, String>()
            testSetting["redis.host"] = getEnv("TEST_REDIS_HOST", "127.0.0.1")
            kotlin.run {
                RedisFactory(testSetting).create().apply {
                    resource.use { it.ping() }
                }
            }
        }
    }

    private val credentialProvider = TestCredentialProvider.credentialProvider

    private val nextcloudSettings = NextcloudSettings(
        getEnv("NEXTCLOUD_USER_BINDING", "username"),
        getEnv("NEXTCLOUD_PUBLIC_URL", "http://127.0.0.1:8082"),
        getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082"),
        getEnv("NEXTCLOUD_USER", "ncadmin"),
        getEnv("NEXTCLOUD_PWP", "ncadminsecret")
    )

    val restConnection = UnirestInstance(
        Config().setObjectMapper(
            JacksonObjectMapper(
                jacksonObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                    .registerModule(JavaTimeModule())
            )
        )
            .addDefaultHeader("Accept", "application/json")
    )

    internal var scenario: TestScenario? = null

    internal fun startIogPlugin(admin: Boolean, backgroundJobManager: BackgroundJobManager): TestScenario {
        initTestSetup()

        val app = AppBuilder().createApp("test-utilities", "test-utilities", "http://127.0.0.1", emptySet(), 0)
        val auth = AuthorizerApi(
            app, AuthorizationController(mutableSetOf(CredentialAuthorizer("credential", credentialProvider, "http://127.0.0.1")), credentialProvider::getUser, db, ActionController(db)),
            true
        )

        val grouppSyncMock = GroupSyncMock()
        val iogConfig = IogConfig(
            TestCredentialProvider.baseDN,
            db,
            true,
            credentialProvider,
            OPNameConfig(mutableMapOf()),
            backgroundJobManager,
            nextcloudSettings._publicUrl,
            pool,
            auth as ISessionOperations,
            grouppSyncMock,
            false,
            IogPluginConstants.SUPPORT_MEMBERSHIP_HASH_ALGORITHM
        )
        val a = IogPlugin(iogConfig, emptyList(), emptyList())
        val services = mutableListOf<IControllableService>()

        val n = NextcloudService(app, pool, nextcloudSettings, credentialProvider)
        services.add(n)
        val nextcloudObject = n.nextcloud
        val ncUtilities = NCUtilities(n.nextcloud)

        val openProject = OpenProjectService(backgroundJobManager, grouppSyncMock, OpenProjectTestVariables.settings, credentialProvider, 3)
        services.add(openProject)

        a.startApis(app, services)
        val opUtilities = OPUtilities(openProject.openproject, backgroundJobManager, grouppSyncMock)
        scenario = TestScenario("$basepathPrefix:${app.port()}$basepathSuffix", app, services, nextcloudObject, n, openProject.openproject, openProject, a, opUtilities, ncUtilities, grouppSyncMock)
        if (admin)
            loginWithAdminUser()
        else
            loginWithStandardUser()

        scenario!!.ncUtilities.reset()
        scenario!!.opUtilities.reset()
        return scenario!!
    }

    fun loginWithStandardUser() {
        val session = restConnection
            .post("${scenario!!.basePath}/authorizer/credential")
            .body(LoginRequest(TestUser.standardLogin.name, TestUser.standardLogin.password))
            .asObject(SessionAnswer::class.java)

        assertNotNull(session.body)
        assertEquals(TestUser.standardLogin.name, session.body.user.username)
    }

    fun loginWithAdminUser() {
        val session = restConnection
            .post("${scenario!!.basePath}/authorizer/credential")
            .body(LoginRequest(TestUser.adminLogin.name, TestUser.adminLogin.password))
            .asObject(SessionAnswer::class.java)

        assertNotNull(session.body)
        assertEquals(TestUser.adminLogin.name, session.body.user.username)
    }

    private fun initTestSetup() {
        val p = credentialProvider.getUsersAndGroups()
        var recreate = false
        if (p.first.size != 2 || !p.first.any { it.username == TestUser.standardUser.username } || !p.first.any { it.username == TestUser.adminUser.username })
            recreate = true
        else {
            val standard = p.first.find { it.username == TestUser.standardUser.username }!!
            val admin = p.first.find { it.username == TestUser.adminUser.username }!!
            if (!p.first.contains(TestUser.standardUser.copy(id = standard.id, registeredSince = standard.registeredSince)) ||
                !p.first.contains(TestUser.adminUser.copy(id= admin.id, registeredSince = admin.registeredSince))
            ) {
                recreate = true
            }
        }
        if (recreate) {
            credentialProvider.getUsers().forEach { if (it.username != "sso_admin") credentialProvider.deleteUser(it) }
            credentialProvider.createUser(TestUser.standardUser, TestUser.standardLogin.password)
            credentialProvider.createUser(TestUser.adminUser, TestUser.adminLogin.password)
        }

        credentialProvider.getGroups().forEach { credentialProvider.deleteGroup(it) }
    }

    internal data class TestScenario(
        val basePath: String,
        val app: Javalin,
        val services: MutableList<IControllableService>,
        val nextcloud: Nextcloud?,
        val nextcloudService: NextcloudService?,
        val openProject: IOpenProject?,
        val openProjectService: OpenProjectService?,
        val iogPlugin: IogPlugin?,
        val opUtilities: OPUtilities,
        val ncUtilities: NCUtilities,
        val groupSyncMock: GroupSyncMock
    )

    fun createStandardGroups() {
        val schema = scenario!!.iogPlugin!!.iogGroupSchema
        val url = nextcloudSettings._publicUrl
        schema.StandardGroupnameGroupKindRelation.forEach {
            val groupName = it.first
            var description = OPNameConfig(mutableMapOf()).credentialGroupToOpenProjectGroup(groupName)
            val groups = when (schema.identifyNodePatternType(groupName).first) {
                IogGroupSchema.PatternTypes.SimpleMemberAdmin -> schema.simpleAdminPattern.create(CreateGroupConfig(groupName, "Beschreibung von $groupName", emptySet(), it.second, url))
                IogGroupSchema.PatternTypes.MemberAdminInterested -> schema.memberAdminPattern.create(CreateGroupConfig(groupName, "Beschreibung von $groupName", emptySet(), it.second, url))
                else -> listOf(credentialProvider.createGroup(Group("", groupName, "Beschreibung von $description", null, setOf(), setOf(), setOf(), setOf(), setOf(), setOf())))
            }

            runBlocking {
                scenario!!.opUtilities.createGroups(groups)
            }

            scenario!!.ncUtilities.addGroupToGroupFolder(groupName)
        }
        scenario!!.ncUtilities.createStandardFolders()
    }

    internal fun createRGTest() {
        createRG("rg-test", "RG Test")
    }

    internal fun removeRGTest() {
        removeMemberAdminInterested("rg-test", "RG Test", "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}")
    }

    internal fun createRG(name: String, description: String) {
        createMemberAdminInterested(name, description, "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}", OPPName.REGIONAL_GROUP, GroupKind.REGIONAL_GROUP)
    }

    internal fun createProject(name: String, description: String) {
        createMemberAdminInterested(name, description, "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}", OPPName.PROJECTS, GroupKind.PROJECT)
    }

    internal fun createKG(name: String, description: String) {
        createSimpleMemberAdmin(name, description, "${NextcloudFolders.GroupShare}/${NextcloudFolders.KG}", OPPName.COMPETENCE_GROUP, GroupKind.COMPETENCE_GROUP)
    }

    private fun createMemberAdminInterested(name: String, description: String, path: String, parentProjectName: String, groupKind: GroupKind) {
        removeMemberAdminInterested(name, description, path)
        val pat = MemberAdminInterestedPattern(credentialProvider)
        val conf = CreateGroupConfig(name, description, emptySet(), groupKind, nextcloudSettings._publicUrl)
        val groupList = pat.create(conf)
        scenario!!.opUtilities.createProject(name, description, groupList, parentProjectName, groupKind)
        scenario!!.ncUtilities.createFolderStructureIfNecessary(path, description)
        scenario!!.ncUtilities.addGroupToGroupFolder("$name-interested")
        scenario!!.ncUtilities.addGroupToGroupFolder("$name-member")
    }

    private fun createSimpleMemberAdmin(name: String, description: String, path: String, parentProjectName: String, groupKind: GroupKind) {
        removeSimpleAdmin(name, description, path)
        val pat = SimpleMemberAdminPattern(credentialProvider)
        val conf = CreateGroupConfig(name, description, emptySet(), groupKind, nextcloudSettings._publicUrl)
        val groupList = pat.create(conf)
        scenario!!.opUtilities.createProject(name, description, groupList, parentProjectName, groupKind)
        scenario!!.ncUtilities.createFolderStructureIfNecessary(path, description)
        scenario!!.ncUtilities.addGroupToGroupFolder(name)
    }

    fun removeMemberAdminInterested(id: String, folderName: String, path: String) {
        val pat = MemberAdminInterestedPattern(credentialProvider)
        pat.delete(id)
        val groups = pat.getAllMetaDescriptions(id).map { it.name }
        scenario!!.opUtilities.removeProject(id, groups)

        scenario!!.ncUtilities.removeFolderIfExists(path, folderName)
    }

    fun removeSimpleAdmin(id: String, folderName: String, path: String) {
        val pat = SimpleMemberAdminPattern(credentialProvider)
        pat.delete(id)
        val groups = pat.getAllMetaDescriptions(id).map { it.name }
        scenario!!.opUtilities.removeProject(id, groups)
        scenario!!.ncUtilities.removeFolderIfExists(path, folderName)
    }
}
