/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.getEnv
import dev.maximilian.feather.multiservice.openproject.OpenProjectSettings

object OpenProjectTestVariables {
    private val userBinding: String = getEnv("OPENPROJECT_USER_BINDING", "username")
    private val host = getEnv("OPENPROJECT_HOST", "http://127.0.0.1:8081")
    private val ssoAdmin = getEnv("OPENPROJECT_SSO_ADMIN", "sso_admin")
    private val ssoSecret: String = getEnv("OPENPROJECT_SSO_SECRET", "s3cr3t")
    private val authUser = getEnv("OPENPROJECT_USER", "admin")
    private val authPassword: String = getEnv("OPENPROJECT_PASSWORD", "admin")

    internal val settings = OpenProjectSettings(
        userBinding,
        host,
        ssoAdmin,
        ssoSecret,
        authUser,
        authPassword
    )
}
