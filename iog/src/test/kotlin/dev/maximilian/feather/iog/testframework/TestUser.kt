/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import java.time.Instant

data class LoginInfo(
    val name: String,
    val password: String
)

object TestUser {
    val standardLogin = LoginInfo("test", "secret1")
    val adminLogin = LoginInfo("Admin Harry", "secret2")

    val standardUser = User(
        standardLogin.name,
        "test",
        "test2",
        "harry@test.de",
        "Harry",
        "Test@harry.de",
        setOf(),
        Instant.now(),
        setOf(),
        setOf(Permission.USER),
        false
    )
    val adminUser = User(
        adminLogin.name,
        "Admin Harry",
        "Admin Harry TEst",
        "admin-harry@test.de",
        "Harry",
        "Test@admin.de",
        setOf(),
        Instant.now(),
        setOf(),
        setOf(Permission.ADMIN),
        false
    )
}
