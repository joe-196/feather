/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants

class IOGGroupScenario(private val credentialProvider: ICredentialProvider) {

    var ldapIDofInterestedGroup = "not yet defined"
    var ldapIDofIOGMember = "not yet defined"

    fun createBaseGroups() {
        val iPeopleGroup = Group(
            "",
            LdapNames.INTERESTED_PEOPLE,
            "Interested People of IOG",
            null,
            setOf(),
            setOf(),
            setOf(),
            setOf(),
            setOf(),
            setOf()
        )
        ldapIDofInterestedGroup = credentialProvider.createGroup(iPeopleGroup).id

        val iogMemberGroup = Group(
            "",
            LdapNames.IOG_MEMBERS,
            "IOG Member",
            null,
            setOf(),
            setOf(),
            setOf(),
            setOf(),
            setOf(),
            setOf()
        )
        ldapIDofIOGMember = credentialProvider.createGroup(iogMemberGroup).id
    }

    fun createRG(name: String) {

        val rgTest = Group(
            "",
            name,
            "Beschreibung von $name",
            null,
            setOf(),
            setOf(),
            setOf(),
            setOf(),
            setOf(),
            setOf()
        )
        val id = credentialProvider.createGroup(rgTest).id

        val rgTestMemberName = "$name-member"
        val rgTestMember = Group(
            "",
            rgTestMemberName,
            "Beschreibung von $name-member",
            null,
            setOf(),
            setOf(),
            setOf(id, ldapIDofIOGMember),
            setOf(),
            setOf(),
            setOf()
        )
        val memberID = credentialProvider.createGroup(rgTestMember).id

        val rgTestInterestedName = "$name-interested"
        val rgTestInterested = Group(
            "",
            rgTestInterestedName,
            "Beschreibung von $rgTestInterestedName",
            null,
            setOf(),
            setOf(),
            setOf(id, ldapIDofInterestedGroup),
            setOf(),
            setOf(),
            setOf()
        )
        credentialProvider.createGroup(rgTestInterested)

        val rgTestAdminName = "$name-admin"
        val rgTestAdmin = Group(
            "",
            rgTestAdminName,
            "Beschreibung von $rgTestAdminName",
            null,
            setOf(),
            setOf(),
            setOf(id, memberID),
            setOf(),
            setOf(),
            setOf()
        )
        credentialProvider.createGroup(rgTestAdmin)
    }

    fun createSimplePatternForBase(basename: String) {
        val kg = Group(
            "",
            basename,
            "Beschreibung von $basename",
            null,
            setOf(),
            setOf(),
            setOf(ldapIDofIOGMember),
            setOf(),
            setOf(),
            setOf()
        )
        val id = credentialProvider.createGroup(kg).id

        val kgAdmin = Group(
            "",
            basename + IogPluginConstants.admin_suffix,
            "Beschreibung von ${basename + IogPluginConstants.admin_suffix}",
            null,
            setOf(),
            setOf(),
            setOf(id),
            setOf(),
            setOf(),
            setOf()
        )
        credentialProvider.createGroup(kgAdmin)
    }
}
