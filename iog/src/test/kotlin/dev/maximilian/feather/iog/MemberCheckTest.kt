/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.bindings.SupportMemberHashRow
import dev.maximilian.feather.iog.internal.memberCheck.MemberCheck
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.OPGroupNames
import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.IOGGroupScenario
import dev.maximilian.feather.iog.testframework.TestCredentialProvider
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MemberCheckTest {
    companion object {
        private val centralOfficeGroupName = OPGroupNames.CENTRAL_OFFICE.trimIndent()

        private val credentialProvider = TestCredentialProvider.credentialProvider
        // private val credentialProvider = CredentialMock()

        private val db = ApiTestUtilities.db

        private val scenarioLoader = IOGGroupScenario(credentialProvider)

        @AfterAll
        fun cleanup() {
            credentialProvider.close()
        }
    }

    private val testUserID = "harald@test.de"

    @Test
    fun `Test getRegistrationState for unregistered rg-test-member is NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for rg-test-interested is UPGRADE_CAN_BE_OFFERED`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))
        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.UPGRADE_CAN_BE_OFFERED, result)
    }

    @Test
    fun `Test getRegistrationState for gs is NO_SUPPORT_MEMBERSHIP_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase(centralOfficeGroupName)
        scenarioLoader.createRG("iog10")
        val user = createTestUserAndGetLDAPID(setOf(centralOfficeGroupName, "iog10-member"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for gs-admin ist NO_SUPPORT_MEMBERSHIP_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase(centralOfficeGroupName)
        val user = createTestUserAndGetLDAPID(setOf(centralOfficeGroupName + "-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered rg-test-admin is NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered kg-test-admin is NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("kg-test")
        val user = createTestUserAndGetLDAPID(setOf("kg-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered kg-member is NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("kg-test")
        val user = createTestUserAndGetLDAPID(setOf("kg-test"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for duplicate mail hash in support member database is NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("other@test.de"), mc.makeHash(testUserID))
        mc.smr.storeHash(999, mc.makeHash("HaraldDuck01.01.1998"), mc.makeHash("other2@test.de"), mc.makeHash(testUserID))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered as-admin is NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test")
        val user = createTestUserAndGetLDAPID(setOf("as-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered as-member is NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test")
        val user = createTestUserAndGetLDAPID(setOf("as-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for rg-member with second mail in DB is MAIL_REGISTRATION_POSSIBLE `() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("other@test.de"), mc.makeHash(testUserID))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.MAIL_REGISTRATION_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for rg-interested with first mail in DB leads to AUTOMATIC_UPGRADE_POSSIBLE `() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))

        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("other@test.de"), mc.makeHash(testUserID))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.AUTOMATIC_UPGRADE_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for rg-member with first mail in DB is MAIL_REGISTRATION_POSSIBLE `() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val ldapid = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash(testUserID), mc.makeHash("other@test.de"))

        val result = mc.getRegistrationState(ldapid)
        assertEquals(MemberCheck.RegistrationState.MAIL_REGISTRATION_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for registered rg-test-member is REGISTERED_SUPPORT_MEMBER`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("harald@test.de"), mc.makeHash("other@test.de"))

        val result = mc.registerByNameAndBirthdate(mc.makeHash("HaraldTest01.01.1995"), user.id)
        assertTrue(result)
        val result2 = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.REGISTERED_SUPPORT_MEMBER, result2)
    }

    @Test
    fun `Test registerByMail for registered rg-test-member leads to FALSE`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("harald@test.de"), mc.makeHash("other@test.de"))
        mc.registerByMail(user)
        val result2 = mc.registerByMail(user)
        assertFalse(result2)
    }

    @Test
    fun `Test registerByMail for registered rg-test-member doesnt fills database`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("harald@test.de"), mc.makeHash("other@test.de"))
        mc.registerByMail(user)
        val databaseUserBefore = mc.getAllRegisteredSupportMember()
        assertEquals(1, databaseUserBefore.count())
        mc.registerByMail(user)
        val databaseUserAfter = mc.getAllRegisteredSupportMember()
        assertEquals(1, databaseUserAfter.count())
    }

    @Test
    fun `Test registerByMail for unregistered rg-test-member fills database and is TRUE`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("harald@test.de"), mc.makeHash("other@test.de"))

        val result = mc.registerByMail(user)
        val databaseUserAfter = mc.getAllRegisteredSupportMember()
        assertTrue(result)
        assertEquals(1, databaseUserAfter.count())
    }

    @Test
    fun `Test deleteFromSupportMemberRegistration really deletes`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("harald@test.de"), mc.makeHash("other@test.de"))

        mc.registerByMail(user)
        mc.userDeleted(user)
        val database = mc.getAllRegisteredSupportMember()
        assertEquals(0, database.count())
    }

    @Test
    fun `Test getAllSupportMembers delivers correct results`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("harald@test.de"), mc.makeHash("other@test.de"))
        mc.smr.storeHash(999, mc.makeHash("HaraldDuck01.01.1998"), mc.makeHash("other2@test.de"), mc.makeHash("other3@test.de"))

        mc.registerByMail(user)
        val databaseUserAfter = mc.getAllRegisteredSupportMember()
        assertEquals(1, databaseUserAfter.count())
        assertEquals("Harald", databaseUserAfter[0].firstName)
        assertEquals("Test", databaseUserAfter[0].lastName)
        assertEquals(user.id, databaseUserAfter[0].ldapID)
        assertEquals(777, databaseUserAfter[0].externalID)
    }

    @Test
    fun `Test getAllSupportMembersOfFundraisingBox delivers correct results`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(777, mc.makeHash("HaraldTest01.01.1995"), mc.makeHash("notworking@test.de"), mc.makeHash("other@test.de"))

        val databaseUserAfter = mc.getAllSupportMembersOfFundraisingBox()
        assertEquals(1, databaseUserAfter.count())
        assertEquals(mc.makeHash("HaraldTest01.01.1995"), databaseUserAfter[0].NameBirthdayHash)
        assertEquals(mc.makeHash("notworking@test.de"), databaseUserAfter[0].MailHash1)
        assertEquals(mc.makeHash("other@test.de"), databaseUserAfter[0].MailHash2)
        assertEquals(777, databaseUserAfter[0].ExternalID)
    }

    @Test
    fun `Test upgrade from rg-test-interested leads to rg-test-member`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))

        mc.upgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)
        val targetID = getIDFromGroupsList("rg-test-member")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test downgrade from rg-test-member leads to rg-test-interested`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        mc.downgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)
        val targetID = getIDFromGroupsList("rg-test-interested")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test downgrade from rg-test-admin leads to rg-test-interested`() {
        val mc = loadDefaults()
        scenarioLoader.createRG("rg-test")
        val user = createTestUserAndGetLDAPID(setOf("rg-test-admin"))

        mc.downgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)

        val targetID = getIDFromGroupsList("rg-test-interested")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test downgrade from rg-test-member with as-membership leads to rg-test-interested`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test1")
        scenarioLoader.createRG("rg-test2")
        val user = createTestUserAndGetLDAPID(setOf("as-test1", "rg-test2-member"))

        mc.downgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)

        val targetID = getIDFromGroupsList("rg-test2-interested")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test updateDatabase with 3 members leads to filled databse with 3 members`() {
        val mc = loadDefaults()
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(SupportMemberHashRow(19, mc.makeHash("askdfjas2"), mc.makeHash("asfdg"), mc.makeHash("SDFsdf99")))
        registered.add(SupportMemberHashRow(22, mc.makeHash("askdfjas3"), mc.makeHash("asfdg12"), mc.makeHash("SDFsdf94")))

        mc.updateDatabase(registered)
        assertEquals(3, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    @Test
    fun `Test number of support member after two updateDatabase calls`() {
        val mc = loadDefaults()
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(SupportMemberHashRow(19, mc.makeHash("askdfjas2"), mc.makeHash("asfdg"), mc.makeHash("SDFsdf99")))
        registered.add(SupportMemberHashRow(22, mc.makeHash("askdfjas3"), mc.makeHash("asfdg12"), mc.makeHash("SDFsdf94")))

        mc.updateDatabase(registered)
        registered.add(SupportMemberHashRow(25, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(SupportMemberHashRow(26, mc.makeHash("askdfjas2"), mc.makeHash("asfdg"), mc.makeHash("SDFsdf99")))
        registered.add(SupportMemberHashRow(77, mc.makeHash("askdfjas3"), mc.makeHash("asfdg12"), mc.makeHash("SDFsdf94")))

        mc.updateDatabase(registered)
        assertEquals(6, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    @Test
    fun `Test updateDatabase removes an unregistered support member`() {
        val mc = loadDefaults()
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(SupportMemberHashRow(19, mc.makeHash("askdfjas2"), mc.makeHash("asfdg"), mc.makeHash("SDFsdf99")))
        registered.add(SupportMemberHashRow(22, mc.makeHash("askdfjas3"), mc.makeHash("asfdg12"), mc.makeHash("SDFsdf94")))

        mc.updateDatabase(registered)
        registered.clear()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(SupportMemberHashRow(26, mc.makeHash("askdfjas2"), mc.makeHash("asfdg"), mc.makeHash("SDFsdf99")))
        mc.updateDatabase(registered)
        assertEquals(2, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    @Test
    fun `Test updateDatabase removes a registered support member`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test1")
        scenarioLoader.createRG("rg-test2")
        val user = createTestUserAndGetLDAPID(setOf("as-test1", "rg-test2-member"))
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(SupportMemberHashRow(7, mc.makeHash("askdfjas2"), mc.makeHash("asfdg"), mc.makeHash("SDFsdf99")))
        registered.add(SupportMemberHashRow(15, mc.makeHash("testuser"), mc.makeHash(user.mail), mc.makeHash("SDFsdf5")))
        registered.add(SupportMemberHashRow(22, mc.makeHash("askdfjas3"), mc.makeHash("asfdg12"), mc.makeHash("SDFsdf94")))

        mc.updateDatabase(registered)
        mc.registerByMail(user)
        assertEquals(3, mc.smr.countSupportMember())
        assertEquals(1, mc.smr.countRegisteredDKPUser())

        registered.clear()
        registered.add(SupportMemberHashRow(99, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(SupportMemberHashRow(26, mc.makeHash("askdfjas2"), mc.makeHash("asfdg"), mc.makeHash("SDFsdf99")))
        mc.updateDatabase(registered)
        assertEquals(2, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    private fun getIDFromGroupsList(name: String): String {
        val allGroups = credentialProvider.getGroups()
        return allGroups.first { it.name == name }.id
    }

    private fun createTestUserAndGetLDAPID(groupOfTestUserNames: Set<String>): User {
        val groupsOfTestUser = mutableListOf<String>()
        groupOfTestUserNames.forEach {
            groupsOfTestUser.add(getIDFromGroupsList(it))
        }

        val newUser = User(
            "",
            "Harald Test",
            "Harry",
            "Harald",
            "Test",
            "harald@test.de",
            groupsOfTestUser.toSet(),
            Instant.now(),
            setOf(),
            setOf(),
            false
        )
        return credentialProvider.createUser(newUser)
    }

    private fun loadDefaults(): MemberCheck {
        val user = credentialProvider.getUsers()
        user.forEach { credentialProvider.deleteUser(it) }
        val groups = credentialProvider.getGroups()
        groups.forEach { credentialProvider.deleteGroup(it) }
        val supportMemberRegister = SupportMemberDB(db)
        supportMemberRegister.deleteAll()
        val mc = MemberCheck(credentialProvider, supportMemberRegister, IogPluginConstants.SUPPORT_MEMBERSHIP_HASH_ALGORITHM, LdapNames.CENTRAL_OFFICE)
        mc.databaseValidation.minSupportMemberCount = 1
        scenarioLoader.createBaseGroups()
        return mc
    }
}
