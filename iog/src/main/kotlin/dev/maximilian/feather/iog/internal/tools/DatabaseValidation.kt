/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.tools

import dev.maximilian.feather.iog.api.bindings.SupportMemberHashRow
import dev.maximilian.feather.iog.settings.IogPluginConstants
import java.security.MessageDigest

internal class DatabaseValidation(hashAlgorithm: String = IogPluginConstants.SUPPORT_MEMBERSHIP_HASH_ALGORITHM) {

    private val md = MessageDigest.getInstance(hashAlgorithm)!!

    var minSupportMemberCount = 5

    fun makeHash(s: String): String {
        val bytes = s.toByteArray()
        val digest = md.digest(bytes)
        return digest.fold("", { str, it -> str + "%02x".format(it) })
    }

    enum class ValidationResult {
        OK,
        NOT_ENOUGH_SUPPORT_MEMBER,
        NAME_HASH_SIZE_INVALID,
        NAME_HASH_EMPTY,
        MAIL_HASH_SIZE_INVALID,
        EXTERNAL_ID_NOT_UNIQUE,
        NOT_ENOUGH_UNIQUE_MAIL_ADDRESS
    }

    fun validate(hashEntries: List<SupportMemberHashRow>): ValidationResult {
        if (hashEntries.count() < minSupportMemberCount) {
            return ValidationResult.NOT_ENOUGH_SUPPORT_MEMBER
        }
        val emptyHash = makeHash("")
        hashEntries.forEach {
            if (it.mailHash1.length != 64 || it.mailHash2.length != 64) {
                return ValidationResult.MAIL_HASH_SIZE_INVALID
            }
        }
        hashEntries.forEach {
            if (it.nameBirthdayHash.length != 64) {
                return ValidationResult.NAME_HASH_SIZE_INVALID
            }
        }
        hashEntries.forEach {
            if (it.nameBirthdayHash == emptyHash) {
                return ValidationResult.NAME_HASH_EMPTY
            }
        }
        if (hashEntries.map { it.externalID }.distinct().count() != hashEntries.map { it.externalID }.count()) {
            return ValidationResult.EXTERNAL_ID_NOT_UNIQUE
        }

        val allMail = hashEntries.map { it.mailHash1 }.filter { it != emptyHash }.toMutableList()
        allMail.addAll(hashEntries.map { it.mailHash2 }.filter { it != emptyHash })
        if (allMail.distinct().count() < minSupportMemberCount) {
            return ValidationResult.NOT_ENOUGH_UNIQUE_MAIL_ADDRESS
        }
        return ValidationResult.OK
    }
}
