/*
*    Copyright [2020-2021] Feather development team, see AUTHORS.md
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package dev.maximilian.feather.iog.api

import dev.maximilian.feather.Permission
import dev.maximilian.feather.iog.api.bindings.SupportMemberRegistrationRequest
import dev.maximilian.feather.iog.api.bindings.UpdateDatabaseRequest
import dev.maximilian.feather.iog.internal.memberCheck.MemberCheck
import dev.maximilian.feather.iog.internal.tools.DatabaseValidation
import dev.maximilian.feather.session
import dev.maximilian.feather.sessionOrMinisession
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.core.security.SecurityUtil.roles
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.plugin.openapi.annotations.OpenApi
import io.javalin.plugin.openapi.annotations.OpenApiContent
import io.javalin.plugin.openapi.annotations.OpenApiRequestBody
import io.javalin.plugin.openapi.annotations.OpenApiResponse
import mu.KotlinLogging

internal class SupportMembershipApi(
    app: Javalin,
    private val memberCheck: MemberCheck
) {
    init {
        app.routes {
            path("bindings/iog") {
                get("/users/verify", ::handleSupportMemberVerification)
                get("/users/registered", ::handleGetAllRegisteredSupportMember, roles(Permission.ADMIN))
                get("/users/hash", ::handleGetDatabaseSupportMember, roles(Permission.ADMIN))
                get("/users/simulate", ::handleSimulate, roles(Permission.ADMIN))
                post("/users/registerByMail", ::handleRegisterByMail)
                post("/users/registerByBirthdate", ::handleSupportMemberRegistrationByNameAndBirthdate)
                post("/users/downgrade", ::handleDowngradeUserToInterested)
                post("/users/upgradeByMail", ::handleUpgradeToSupportMemberByMail)
                post("/users/upgradeByBirthdate", ::handleUpgradeToSupportMemberByNameBirthdate)
                post(
                    "/users/updateDB",
                    ::handleUpdateSupportMemberDatabase,
                    roles(Permission.UPLOAD_SUPPORT_MEMBER, Permission.ADMIN)
                )
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    @OpenApi(
        summary = "Check if a name birthday hash registration process is required.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleSupportMemberVerification(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not verify user due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            try {
                val result = memberCheck.getRegistrationState(session.user)
                if ((result == MemberCheck.RegistrationState.MAIL_REGISTRATION_POSSIBLE) ||
                    (result == MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED)
                ) {
                    logger.info { "User ${session.user.displayName} blocked: $result" }
                    ctx.status(403)
                } else if ((result == MemberCheck.RegistrationState.AUTOMATIC_UPGRADE_POSSIBLE) ||
                    (result == MemberCheck.RegistrationState.UPGRADE_CAN_BE_OFFERED)
                ) {
                    logger.info { "User ${session.user.displayName} can upgrade: $result" }
                    ctx.status(201)
                } else {
                    logger.info { "User accepted: $result" }
                    ctx.status(200)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Failed to verify user due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Register a support member by name and birthdate hash.",
        requestBody = OpenApiRequestBody([OpenApiContent(SupportMemberRegistrationRequest::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleSupportMemberRegistrationByNameAndBirthdate(ctx: Context) {
        val session = ctx.sessionOrMinisession()

        val body = ctx.body<SupportMemberRegistrationRequest>()
        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not register user due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            try {
                val result = memberCheck.registerByNameAndBirthdate(body.nameBirthdayHash, session.user.id)
                if (result) {
                    logger.info { "User ${session.user.displayName} registered as support member by name and birthday." }
                    ctx.status(200)
                } else {
                    logger.info { "User ${session.user.displayName} could not be registered." }
                    ctx.status(403)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Failed to verify user due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Downgrade a person from *-member groupto *-interested group and remove from all special groups.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleDowngradeUserToInterested(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not downgrade user due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            try {
                memberCheck.downgrade(session.user.id)
                logger.info { "User ${session.user.displayName} downgraded to interested." }
                ctx.status(200)
            } catch (e: Exception) {
                logger.warn(e) { "Failed to downgrade user due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Try to register an support member by mail hash.",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleRegisterByMail(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not downgrade user due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            try {
                val result = memberCheck.registerByMail(session.user)
                if (result) {
                    logger.info { "User ${session.user.displayName} successful registered as support member by mail." }
                    ctx.status(200)
                } else {
                    logger.info { "User ${session.user.displayName} could not register support member by mail." }
                    ctx.status(403)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Failed to upgrade user by mail due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Try to register an interested member as support member by mail hash. Upgrade *-interested group to *-member group after success. ",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleUpgradeToSupportMemberByMail(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not downgrade user due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            try {
                val result = memberCheck.checkAndUpgrade(session.user.id)
                if (result) {
                    logger.info { "User ${session.user.displayName} successful upgrade to support member by mail." }
                    ctx.status(200)
                } else {
                    logger.info { "User ${session.user.displayName} could not upgrade to support member by mail." }
                    ctx.status(403)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Failed to upgrade user by mail due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Try to register an interested member as support member by name and birthday hash. Upgrade *-interested group to *-member group after success. ",
        requestBody = OpenApiRequestBody([OpenApiContent(SupportMemberRegistrationRequest::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )

    private fun handleUpgradeToSupportMemberByNameBirthdate(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val errorMessages = mutableListOf<String>()

        val body = ctx.body<SupportMemberRegistrationRequest>()
        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not downgrade user due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            try {
                val result = memberCheck.registerByNameAndBirthdate(body.nameBirthdayHash, session.user.id)
                if (result) {
                    memberCheck.upgrade(session.user.id)
                    logger.info { "User ${session.user.displayName} successful upgrade to support member by name and birthdate." }
                    ctx.status(200)
                    ctx.json(result)
                } else {
                    logger.info { "User ${session.user.displayName} could not upgrade to support member by name and birthdate." }
                    ctx.status(403)
                    ctx.json(result)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Failed to upgrade user by mail due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Upload support member database. Needs ADMIN or UPLOAD_SUPPORT_MEMBER permission. At least 5 unique support members required.",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["UPLOAD_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400")]
    )
    private fun handleUpdateSupportMemberDatabase(ctx: Context) {
        val session = ctx.session()
        val errorMessages = mutableListOf<String>()

        val creator = session.user
        if (!creator.permissions.contains(Permission.UPLOAD_SUPPORT_MEMBER) && !creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        val body = ctx.body<UpdateDatabaseRequest>()
        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not upload database due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            try {
                memberCheck.databaseValidation.minSupportMemberCount = 5
                val result = memberCheck.databaseValidation.validate(body.hashEntries)

                if (result == DatabaseValidation.ValidationResult.OK) {
                    memberCheck.updateDatabase(body.hashEntries)
                    logger.info { "Database successfully updated with ${body.hashEntries.count()} entries" }
                    ctx.status(200)
                } else {
                    var reason = when (result) {
                        DatabaseValidation.ValidationResult.NOT_ENOUGH_SUPPORT_MEMBER -> "Not enough support members in database. Minimum is ${memberCheck.databaseValidation.minSupportMemberCount}. Upload rejected."
                        DatabaseValidation.ValidationResult.EXTERNAL_ID_NOT_UNIQUE -> "The external IDs are not unique."
                        DatabaseValidation.ValidationResult.MAIL_HASH_SIZE_INVALID -> "The email hash size is not 64 signs."
                        DatabaseValidation.ValidationResult.NOT_ENOUGH_UNIQUE_MAIL_ADDRESS -> "The database does not contain at least ${memberCheck.databaseValidation.minSupportMemberCount} unique mail addresses."
                        DatabaseValidation.ValidationResult.NAME_HASH_SIZE_INVALID -> "Size of a name birthday hash is not 64 signs"
                        DatabaseValidation.ValidationResult.NAME_HASH_EMPTY -> "Name hash is hash of an empty string"
                        else -> "unknown database validation result $result"
                    }

                    errorMessages += reason
                    logger.warn { "MultiServiceApi::handleUpdateSupportMemberDatabase $reason. Upload REJECTED." }
                    ctx.status(400)
                    ctx.json(errorMessages)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Failed to upgrade user by mail due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
            }
        }
    }

    @OpenApi(
        summary = "Get all registered support member.",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["DUMP_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleGetAllRegisteredSupportMember(ctx: Context) {
        val session = ctx.session()
        val errorMessages = mutableListOf<String>()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        try {
            val allUser = memberCheck.getAllRegisteredSupportMember()
            logger.info { "Dumped ${allUser.count()} entries" }
            ctx.status(200)
            ctx.json(allUser)
        } catch (e: Exception) {
            logger.warn(e) { "Failed to get all registered support members due to exception" }

            errorMessages += "Interner Fehler (Benachrichtige die IT)"
            ctx.status(400)
            ctx.json(errorMessages)
        }
    }

    @OpenApi(
        summary = "Get all support members of fundraising box database.",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["DUMP_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleGetDatabaseSupportMember(ctx: Context) {
        val session = ctx.session()
        val errorMessages = mutableListOf<String>()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        try {
            val allUser = memberCheck.getAllSupportMembersOfFundraisingBox()
            logger.info { "Dumped ${allUser.count()} entries" }
            ctx.status(200)
            ctx.json(allUser)
        } catch (e: Exception) {
            logger.warn(e) { "Failed to get all registered support members due to exception" }

            errorMessages += "Interner Fehler (Benachrichtige die IT)"
            ctx.status(400)
            ctx.json(errorMessages)
        }
    }

    @OpenApi(
        summary = "Get all users that could be registered by mail",
        requestBody = OpenApiRequestBody([OpenApiContent(UpdateDatabaseRequest::class)]),
        tags = ["DUMP_SUPPORT_MEMBER"],
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")]
    )
    private fun handleSimulate(ctx: Context) {
        val session = ctx.session()
        val errorMessages = mutableListOf<String>()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        try {
            val allUser = memberCheck.getSimulation()
            logger.info { "Dumped ${allUser.count()} entries" }
            ctx.status(200)
            ctx.json(allUser)
        } catch (e: Exception) {
            logger.warn(e) { "Failed to get users that could be registered by mail automatically" }

            errorMessages += "Interner Fehler (Benachrichtige die IT)"
            ctx.status(400)
            ctx.json(errorMessages)
        }
    }
}
