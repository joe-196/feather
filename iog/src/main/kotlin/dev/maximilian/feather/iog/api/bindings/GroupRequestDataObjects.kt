/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package dev.maximilian.feather.iog.api.bindings

internal data class IogGroupCreateRequest(
    val displayName: String,
    val description: String,
    val kind: String,
    val owners: Set<String> = emptySet(),
    val verified: Boolean
)

internal data class IogGroupDetailsRequest(
    val groups: List<String>
)

internal data class IogGroupDeleteRequest(
    val password: String,
    val verified: Boolean
)

internal data class IogGroupsDeleteRequest(
    val password: String,
    val groups: List<String>,
    val verified: Boolean
)
