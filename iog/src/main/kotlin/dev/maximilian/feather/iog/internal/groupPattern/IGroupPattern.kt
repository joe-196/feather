/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.groupPattern

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription

internal interface IGroupPattern {
    fun create(config: CreateGroupConfig): List<Group>
    fun search(prefixedName: String): List<Group>
    fun getMetaDescription(groupName: String): GroupMetaDescription
    fun getAllMetaDescriptions(groupName: String): List<GroupMetaDescription>
    fun isAdminGroup(groupName: String): Boolean
    fun isMainGroup(groupName: String): Boolean
    fun userMembersAllowed(groupName: String): Boolean
    fun delete(groupName: String)
}
