/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.supportmember

import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB.DKPUserMapping.externalID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.replace
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

internal class SupportMemberDB(val db: Database) {
    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(DKPUserMapping)
            SchemaUtils.createMissingTablesAndColumns(SupportMember)
        }
    }

    object DKPUserMapping : IntIdTable() {
        val externalID = integer("externalID").uniqueIndex()
        val ldapID = varchar("ldapID", 50)
    }

    object SupportMember : IntIdTable() {
        val externalID = integer("externalID")
        val nameBirthdateHash = varchar("nameBirthdateHash", 65)
        val mailHash1 = varchar("mailHash1", 65)
        val mailHash2 = varchar("mailHash2", 65)
        override val primaryKey: PrimaryKey
            get() = PrimaryKey(externalID)
    }

    fun getExternalIDbyMail(mailHash: String): Int? = transaction(db) {
        SupportMember.select { (SupportMember.mailHash1 eq mailHash) or (SupportMember.mailHash2 eq mailHash) }.singleOrNull()?.get(
            SupportMember.externalID
        )
    }

    fun countSupportMember(): Long = transaction(db) {
        SupportMember.selectAll().count()
    }

    fun countRegisteredDKPUser(): Long = transaction(db) {
        DKPUserMapping.selectAll().count()
    }

    fun getExternalIDbyLDAP(ldapID: String): Int? = transaction(db) {
        DKPUserMapping.select { DKPUserMapping.ldapID eq ldapID }.singleOrNull()?.get(externalID)
    }

    fun storeLDAPandExternalID(ldapID: String, externalID: Int) = transaction(db) {
        DKPUserMapping.insert {
            it[DKPUserMapping.externalID] = externalID
            it[DKPUserMapping.ldapID] = ldapID
        }
    }

    fun countMailHash(mailHash: String): Int = transaction(db) {
        SupportMember.select { (SupportMember.mailHash1 eq mailHash) or (SupportMember.mailHash2 eq mailHash) }.count().toInt()
    }

    fun getExternalIDbyNameHash(thisNameBirthdateHash: String): Int? = transaction(db) {
        SupportMember.select { SupportMember.nameBirthdateHash eq thisNameBirthdateHash }.singleOrNull()?.get(
            SupportMember.externalID
        )
    }

    fun storeHash(externID: Int, nameBirthdayHash: String, mailHash: String, alternativeMailHash: String) = transaction(db) {
        SupportMember.replace {
            it[externalID] = externID
            it[nameBirthdateHash] = nameBirthdayHash
            it[mailHash1] = mailHash
            it[mailHash2] = alternativeMailHash
        }
    }

    fun deleteAll() = transaction(db) {
        SupportMember.deleteAll()
        DKPUserMapping.deleteAll()
    }

    fun deleteSupportMembership(externalID: Int) = transaction(db) {
        DKPUserMapping.deleteWhere { DKPUserMapping.externalID eq externalID }
        SupportMember.deleteWhere { SupportMember.externalID eq externalID }
    }

    fun getAllRegisteredExternalID() = transaction(db) {
        DKPUserMapping.selectAll().orderBy(DKPUserMapping.externalID).map { it[DKPUserMapping.externalID] }
    }

    fun getAllSupportMemberExternalID() = transaction(db) {
        SupportMember.selectAll().orderBy(SupportMember.externalID).map { it[SupportMember.externalID] }
    }

    fun getAllRegistered(): List<Pair<String, Int>> = transaction(db) {
        DKPUserMapping.selectAll().orderBy(DKPUserMapping.ldapID).map { Pair(it[DKPUserMapping.ldapID], it[DKPUserMapping.externalID]) }
    }

    fun getAllSupportMember() = transaction(db) {
        SupportMember.selectAll().orderBy(SupportMember.externalID).map { SupportMember(it[SupportMember.externalID], it[SupportMember.nameBirthdateHash], it[SupportMember.mailHash1], it[SupportMember.mailHash2]) }
    }

    fun deleteUserRegistration(ldapID: String) = transaction(db) {
        DKPUserMapping.deleteWhere { DKPUserMapping.ldapID eq ldapID }
    }
}
