/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package dev.maximilian.feather.iog.internal.group.plausibility

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.InputInternalPublicFolder
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.groupPattern.IGroupPattern
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.tools.NextcloudFolderCreator
import dev.maximilian.feather.iog.internal.tools.PublicLinkConverter
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.openproject.IOpenProject
import java.util.UUID

internal class NCPlausibility(
    nc: Nextcloud,
    private val backgroundJobManager: BackgroundJobManager,
    private val publicLink: String,
    private val credentials: ICredentialProvider,
    private val jobID: UUID
) {
    private val repairable = RepairMessages.AUTO
    private val repaired = RepairMessages.REPAIR_DONE
    private val notRepairable = RepairMessages.MANUAL

    private val fileSystem = SelfCheckingFileSystem(FileSystemCached(nc))
    private val groupFolderPermissions = fileSystem.findGroupFolder(NextcloudFolders.GroupShare)?.groups
        ?: throw Exception("NCPlausibility::init cannot find IOG groupfolder in Nextcloud (${NextcloudFolders.GroupShare})")

    internal suspend fun check(
        group: Group,
        patternType: Pair<IogGroupSchema.PatternTypes, GroupKind>,
        pattern: IGroupPattern,
        op: IOpenProject,
        autoRepair: Boolean
    ): MutableList<String> {
        val groupKind = patternType.second
        val errorList = mutableListOf<String>()
        if (pattern.isMainGroup(group.name)) {
            val nfc = NextcloudFolderCreator(fileSystem, jobID, backgroundJobManager, publicLink)

            var description =
                when (group.name) {
                    LdapNames.OMS -> NextcloudFolders.OMS
                    LdapNames.CENTRAL_OFFICE -> NextcloudFolders.CentralOffice
                    LdapNames.ASSOCIATION_BOARD -> NextcloudFolders.AssociationBoard
                    else ->
                        if (patternType.first == IogGroupSchema.PatternTypes.MemberAdminInterested)
                            pattern.getMetaDescription(group.name).descriptionWithoutPrefix
                        else ""
                }
            val path = nfc.getParentFolder(groupKind, description).let {
                completePath ->
                completePath.substring(0, completePath.indexOfLast { char -> char == '/' })
            }.toString()

            val createdLdapGroups = pattern.getAllMetaDescriptions(group.name).mapNotNull { credentials.getGroupByName(it.name) }
            val accessToBeAssigned = nfc.getAccessToBeAssigned(groupKind, createdLdapGroups.size)
            createdLdapGroups.zip(accessToBeAssigned).forEach {
                if (it.second != null) {
                    val permissionType: PermissionType = when {
                        it.second!! -> PermissionType.all
                        else -> PermissionType.readWrite
                    }
                    if (!groupFolderPermissions.containsKey(it.first.name)) {
                        errorList.add("${it.first.name} wurde nicht zum Groupfolder in Nextcloud hinzugefügt.")
                        if (autoRepair) {
                            fileSystem.addGroupToGroupFolder(
                                fileSystem.findGroupFolder(NextcloudFolders.GroupShare)!!,
                                it.first.name,
                                permissionType
                            )
                            errorList.add("$repaired.")
                        } else errorList.add("$repairable.")
                    } else if (groupFolderPermissions[it.first.name] != permissionType.intValue) {
                        errorList.add("Berechtigung von ${it.first.name} in GroupFolders ist falsch. Es sollte $permissionType sein, ist jedoch ${groupFolderPermissions[it.first.name]!!}.")
                        if (autoRepair) {
                            val g = fileSystem.findGroupFolder(NextcloudFolders.GroupShare)!!
                            fileSystem.removeGroupFromGroupFolder(g, it.first.name)
                            fileSystem.addGroupToGroupFolder(g, it.first.name, permissionType)
                            errorList.add("$repaired.")
                        } else errorList.add("$repairable.")
                    }
                }
            }
            if (description == "")
                description = op.getProjectByIdentifierOrName(group.name)?.name ?: ""
            if (description != "") {
                val childFolders = fileSystem.listFolders(path)
                if (childFolders.contains(description)) {
                    val memberGroupName = nfc.getMemberGroupName(groupKind, createdLdapGroups).name
                    val iipf = InputInternalPublicFolder(fileSystem, "$path/$description", group.name, memberGroupName, PublicLinkConverter(publicLink), groupKind == GroupKind.PROJECT)
                    errorList.addAll(iipf.verify(autoRepair))
                } else errorList.add("Konnte in NC folgendes Verzeichnis nicht finden: <$path/$description> $notRepairable.")
            } else errorList.add("Die folgende Gruppe kann nicht geprüft werden: <${group.name}> $notRepairable.")
        }
        return errorList
    }
}
