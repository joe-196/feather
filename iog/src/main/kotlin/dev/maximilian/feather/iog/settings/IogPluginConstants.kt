/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.settings

object IogPluginConstants {
    const val member_suffix = "-member"
    const val interested_suffix = "-interested"
    const val admin_suffix = "-admin"
    const val rg_prefix = "rg-"
    const val kg_prefix = "kg-"
    const val as_prefix = "as-"
    const val bila_suffix = "-bila"
    const val prfr_suffix = "-prfr"
    const val program_prefix = "progr-"
    const val anker_prefix = "ankerpartner-"

    const val REGIONAL_GROUP_TEMPLATE = ""
    const val BILA_GROUP_TEMPLATE = ""
    const val PR_FR_GROUP_TEMPLATE = ""
    const val COMPETENCE_GROUP_TEMPLATE = ""
    const val COMMITTEE_TEMPLATE = ""
    const val COLLABORATION_TEMPLATE = ""
    const val PROJECT_TEMPLATE = ""
    const val SANDBOX_PROJECT_ID = "sandbox"
    const val TEMPLATE_ONLY = "work_packages"

    const val SUPPORT_MEMBERSHIP_HASH_ALGORITHM = "SHA-256"
}
