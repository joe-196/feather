/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.settings

import dev.maximilian.feather.iog.api.bindings.MultiServiceGroupKindDefinition

object IogServiceApiConstants {
    internal const val GROUPS_PER_MEMBER_ADMIN_INTERESTED_PATTERN = 4
    internal const val GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN = 2

    private val REGIONAL_GROUP = MultiServiceGroupKindDefinition(
        text = "Regionalgruppe",
        value = GroupKind.REGIONAL_GROUP.toString(),
        description = "Es werden die Gruppen 'rg-<name>', 'rg-<name>-member', 'rg-<name>-interested' und 'rg-<name>-admin' und ein Projekt <name> ausgehend vom Regionalgruppen-Template angelegt.",
        applicable = true,
        adminNeeded = true,
        relatedGroupNeeded = false
    )
    private val BILA_GROUP = MultiServiceGroupKindDefinition(
        text = "BiLa-Gruppe",
        value = GroupKind.BILA_GROUP.toString(),
        description = "Es werden OpenProject- und NextCloud-Strukturen im Strukturpunkt BiLa für eine existierende RG angelegt.",
        applicable = true,
        adminNeeded = false,
        relatedGroupNeeded = true
    )
    private val PR_FR_GROUP = MultiServiceGroupKindDefinition(
        text = "PR-FR-Gruppe",
        value = GroupKind.PR_FR_GROUP.toString(),
        description = "Es werden OpenProject- und NextCloud-Strukturen im Strukturpunkt PR-FR für eine existierende RG angelegt.",
        applicable = true,
        adminNeeded = false,
        relatedGroupNeeded = true
    )
    private val COMPETENCE_GROUP = MultiServiceGroupKindDefinition(
        text = "Kompetenzgruppe",
        value = GroupKind.COMPETENCE_GROUP.toString(),
        description = "Es werden die Gruppen 'kg-<name>' und 'kg-<name>-admin' und ein Projekt <name> ausgehend vom Kompetenzgruppen-Template angelegt. Bitte Admin-Gruppe für Event-Kalender freischalten!",
        applicable = true,
        adminNeeded = true,
        relatedGroupNeeded = false
    )
    private val COMMITTEE = MultiServiceGroupKindDefinition(
        text = "Ausschuss",
        value = GroupKind.COMMITTEE.toString(),
        description = "Es werden die Gruppen 'as-<name>' und 'as-<name>-admin' und ein Projekt <name> ausgehend vom Ausschuss-Template angelegt. Bitte Admin-Gruppe für Event-Kalender freischalten!",
        applicable = true,
        adminNeeded = true,
        relatedGroupNeeded = false
    )
    private val COLLABORATION = MultiServiceGroupKindDefinition(
        text = "Projektübergreifende Zusammenarbeit",
        value = GroupKind.COLLABORATION.toString(),
        description = "Es werden die Gruppen 'as-<name>' und 'as-<name>-admin' und ein Projekt <name> ausgehend vom P.-Zusammenarbeit-Template angelegt.",
        applicable = true,
        adminNeeded = true,
        relatedGroupNeeded = false
    )
    private val PROJECT = MultiServiceGroupKindDefinition(
        text = "Auslandsprojekt",
        value = GroupKind.PROJECT.toString(),
        description = "Es werden die Gruppen '<name>', '<name>-member', '<name>-interested' und '<name>-admin' und ein Projekt <name> ausgehend vom Auslandsprojekt-Template angelegt.",
        applicable = true,
        adminNeeded = true,
        relatedGroupNeeded = false
    )
    private val NO_SPECIALITY_MEMBER_ADMIN_PUBLIC = MultiServiceGroupKindDefinition(
        text = "ohne Spezialität (ohne Interested) für normalsichtbare Gruppe",
        value = GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC.toString(),
        description = "Es werden die Gruppen '<name>' und '<name>-admin' und ein leeres Projekt angelegt. IogMember ist Reader.",
        applicable = false,
        adminNeeded = false,
        relatedGroupNeeded = false
    )
    private val NO_SPECIALITY_MEMBER_ADMIN_SECRET = MultiServiceGroupKindDefinition(
        text = "ohne Spezialität (ohne Interested) für geschützte Gruppe",
        value = GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET.toString(),
        description = "Es werden die Gruppen '<name>' und '<name>-admin' und ein leeres Projekt für GS, OMs, o.ä. angelegt. IogMember ist Interested.",
        applicable = false,
        adminNeeded = false,
        relatedGroupNeeded = false
    )
    private val NO_SPECIALITY_MEMBER_ADMIN_INTERESTED = MultiServiceGroupKindDefinition(
        text = "ohne Spezialität (mit Interested)",
        value = GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED.toString(),
        description = "Es werden die Gruppen '<name>', '<name>-member', '<name>-interested' und '<name>-admin' und ein leeres Projekt angelegt.",
        applicable = false,
        adminNeeded = false,
        relatedGroupNeeded = false
    )
    private val LDAP_ONLY = MultiServiceGroupKindDefinition(
        text = "nur LDAP",
        value = GroupKind.LDAP_ONLY.toString(),
        description = "Es wird nur die Gruppe '<name>' ohne Projekt angelegt.",
        applicable = false,
        adminNeeded = false,
        relatedGroupNeeded = false
    )

    val GROUP_KINDS = mapOf<GroupKind, MultiServiceGroupKindDefinition>(
        GroupKind.REGIONAL_GROUP to REGIONAL_GROUP,
        GroupKind.BILA_GROUP to BILA_GROUP,
        GroupKind.PR_FR_GROUP to PR_FR_GROUP,
        GroupKind.COMPETENCE_GROUP to COMPETENCE_GROUP,
        GroupKind.COLLABORATION to COLLABORATION,
        GroupKind.COMMITTEE to COMMITTEE,
        GroupKind.PROJECT to PROJECT,
        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET to NO_SPECIALITY_MEMBER_ADMIN_SECRET,
        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC to NO_SPECIALITY_MEMBER_ADMIN_PUBLIC,
        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED to NO_SPECIALITY_MEMBER_ADMIN_INTERESTED,
        GroupKind.LDAP_ONLY to LDAP_ONLY
    )
}
