/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group.plausibility

import dev.maximilian.feather.nextcloud.INextcloud

internal class SelfCheckingFileSystem(private val fs: INextcloud) : INextcloud by fs {
    fun createFolderRecursive(path: String) {
        val pathComponents = path.removeSuffix("/").split('/')
        var alreadyCreatedIndex = 1
        while (alreadyCreatedIndex < pathComponents.size) {
            val basePath = pathComponents.subList(0, alreadyCreatedIndex).joinToString(separator = "/") { it }
            val t = fs.listFiles(basePath)
            if (!t.contains(pathComponents[alreadyCreatedIndex])) {
                do {
                    alreadyCreatedIndex++
                    val createPath = pathComponents.subList(0, alreadyCreatedIndex).joinToString(separator = "/") { it }
                    fs.createFolder(createPath)
                } while (alreadyCreatedIndex < pathComponents.size)
                break
            } else alreadyCreatedIndex++
        }
    }

    fun folderExists(toCheck: String): Boolean {
        val components = toCheck.removeSuffix("/").split('/')
        return folderExistsRecursive(1, components)
    }

    private fun folderExistsRecursive(alreadyCheckedIndex: Int, pathComponents: List<String>): Boolean {
        val basePath = pathComponents.subList(0, alreadyCheckedIndex).joinToString(separator = "/") { it }
        val t = fs.listFiles(basePath)
        if (t.contains(pathComponents[alreadyCheckedIndex])) {
            if (alreadyCheckedIndex + 1 == pathComponents.size)
                return true
            else return folderExistsRecursive(alreadyCheckedIndex + 1, pathComponents)
        } else return false
    }
}
