/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group.plausibility

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.groupPattern.IGroupPattern
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.openproject.OpenProjectMembership
import dev.maximilian.feather.openproject.OpenProjectProject
import dev.maximilian.feather.openproject.OpenProjectRole
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.time.Duration
import java.time.Instant

internal data class OPPlausibilityCheckResult(
    val errorMessages: List<String>,
    val resync: Boolean
)

internal class OpenProjectPlausibility(openProject: IOpenProject, private val credentials: ICredentialProvider) {
    var openProjectGroups: List<OpenProjectGroup>
    var opProjects: List<OpenProjectProject>
    var openProjectMembership: List<OpenProjectMembership>
    var opRoles: List<OpenProjectRole>
    private val repairable = RepairMessages.AUTO
    private val repaired = RepairMessages.REPAIR_DONE
    private val notRepairable = RepairMessages.MANUAL

    companion object : KLogging()

    init {
        val startTime = Instant.now()
        runBlocking {
            opRoles = openProject.getRoles()
            openProjectGroups = openProject.getGroups()
            opProjects = openProject.getProjects()
            openProjectMembership = openProject.getMemberships(opRoles)
        }
        val duration = Duration.between(startTime, Instant.now()).toMillis()
        logger.info {
            "OpenProjectPlausibility::init found ${opProjects.count()} projects, ${opRoles.count()} roles, ${openProjectGroups.count()} groups and ${openProjectMembership.count()} memberships in $duration ms."
        }
    }

    fun check(
        group: Group,
        patternType: Pair<IogGroupSchema.PatternTypes, GroupKind>,
        pattern: IGroupPattern,
        opc: OpenProjectPCreator,
        allUser: Collection<User>,
        autoRepair: Boolean
    ): OPPlausibilityCheckResult {
        var resyncRequired = false
        val errorMessages = mutableListOf<String>()
        if (patternType.first == IogGroupSchema.PatternTypes.SimpleMemberAdmin || patternType.first == IogGroupSchema.PatternTypes.MemberAdminInterested) {
            val name = group.name
            val groupKind = patternType.second
            val nominalGroups = pattern.getAllMetaDescriptions(name)
            openProjectGroups.find { it.name == name }?.also {
                opGroup ->
                if (pattern.isMainGroup(name)) {
                    if (!opProjects.any { p -> p.identifier == name })
                        errorMessages += "Es gibt kein OP Projekt, dass $name als Identifier hat $notRepairable."
                    else if (groupKind == GroupKind.PROJECT || groupKind == GroupKind.REGIONAL_GROUP || groupKind == GroupKind.COMPETENCE_GROUP) {
                        val relatedProjectResult =
                            checkRelatedProjects(name, groupKind, pattern.getMetaDescription(name))
                        errorMessages.addAll(relatedProjectResult.second)

                        relatedProjectResult.first.forEach { r ->
                            val memberShips =
                                openProjectMembership.filter { it.project.value?.identifier == r.identifier }
                            errorMessages.addAll(checkRoles(groupKind, nominalGroups, memberShips, opc))
                        }
                    }
                }
                group.groupMembers.forEach {
                    userID ->
                    allUser.find { it.id == userID }?.let { ldapUser ->
                        if (!opGroup.members.any { opUser -> opUser.value?.email == ldapUser.mail }) {
                            errorMessages += "Die Gruppe ${group.name} enthält in OpenProject nicht den Member ${ldapUser.displayName} (${ldapUser.mail})"
                            if (autoRepair) {
                                resyncRequired = true
                                errorMessages.add("(Autorepair via Resynchronistation möglich).")
                            } else errorMessages.add("$repairable.")
                        }
                    }
                }
                opGroup.members.forEach { opUser ->
                    allUser.find { it.mail == opUser.value?.email }?.let { opUserInLDAP ->
                        if (!group.isUserMemberRecursive(opUserInLDAP.id, credentials)) {
                            errorMessages += "Die Gruppe ${group.name} enthält in OpenProject den zusätzlichen Member $opUser."
                            if (autoRepair) {
                                resyncRequired = true
                                errorMessages.add("(Autorepair via Resynchronistation möglich).")
                            } else errorMessages.add("$repairable.")
                        }
                    } ?: run { errorMessages += "Die Person mit ${opUser.value?.firstName} ${opUser.value?.lastName} (${opUser.value?.email}) aus der OP-Gruppe ${opGroup.name} existiert nicht in LDAP $notRepairable." }
                }
            } ?: run { errorMessages += "Die Gruppe ${group.name} existiert nicht in OpenProject $notRepairable." }
        }
        return OPPlausibilityCheckResult(errorMessages, resyncRequired)
    }

    private fun checkRelatedProjects(
        name: String,
        groupKind: GroupKind,
        meta: GroupMetaDescription
    ): Pair<Collection<OpenProjectProject>, List<String>> {
        val errorMessages = mutableListOf<String>()
        val p = opProjects.find { p -> p.identifier == name }!!
        val pList = mutableListOf(p)

        if (meta.descriptionWithoutPrefix != "" && p.name != meta.descriptionWithoutPrefix)
            errorMessages += "Das OP Projekt hat den Namen ${p.name}. Es muss jedoch <${meta.descriptionWithoutPrefix}> heißen $notRepairable."

        if (groupKind == GroupKind.REGIONAL_GROUP) {
            opProjects.find { it.identifier == name + IogPluginConstants.prfr_suffix }
                ?.let { project ->
                    pList.add(project)
                    meta.descriptionWithoutPrefix.let { s ->
                        if (project.name != "$s (PR-FR)")
                            errorMessages += "Das BiLa Projekt hat den Namen ${project.name}. Es muss jedoch <$s (PR-FR)> heißen $notRepairable."
                    }
                }
            opProjects.find { it.identifier == name + IogPluginConstants.bila_suffix }
                ?.let { project ->
                    pList.add(project)
                    meta.descriptionWithoutPrefix.let { s ->
                        if (project.name != s + " (BiLa)")
                            errorMessages += "Das BiLa Projekt hat den Namen ${project.name}. Es muss jedoch <$s (BiLa)> heißen $notRepairable."
                    }
                }
        }
        return Pair(pList, errorMessages)
    }

    private fun checkRoles(
        groupKind: GroupKind,
        metaGroups: List<GroupMetaDescription>,
        memberShipsInProject: List<OpenProjectMembership>,
        opc: OpenProjectPCreator
    ): List<String> {
        val errorMessages = mutableListOf<String>()

        if (groupKind != GroupKind.LDAP_ONLY) {
            val t = opc.getRolesToBeAssigned(
                groupKind,
                metaGroups.size,
                opRoles
            )
            t.forEachIndexed { index, role ->
                if (role != null) {
                    if (!memberShipsInProject.any { (it.principal.value as? OpenProjectGroup?)?.name == metaGroups[index].name && it.roles.any { it.name == role.name } })
                        errorMessages += "Die ${role.name}-Rolle ist in OP nicht an ${metaGroups[index].name} vergeben."
                } else if (openProjectMembership.any {
                    (it.principal.value as? OpenProjectGroup)?.name == metaGroups[index].name
                }
                )
                    errorMessages += "Die Gruppe ${metaGroups[index].name} darf in OP keine Rolle haben."
            }
        }
        return errorMessages
    }
}
