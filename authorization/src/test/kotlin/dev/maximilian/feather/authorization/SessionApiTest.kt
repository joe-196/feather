/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.AppBuilder
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.authorization.api.LoginRequest
import dev.maximilian.feather.authorization.api.SessionAnswer
import dev.maximilian.feather.authorization.mocks.CredentialMock
import kong.unirest.Config
import kong.unirest.GenericType
import kong.unirest.HttpRequest
import kong.unirest.HttpResponse
import kong.unirest.RequestBodyEntity
import kong.unirest.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import org.eclipse.jetty.http.HttpStatus
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Test
import java.sql.DriverManager
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull

class SessionApiTest {
    companion object {
        private val db =
            Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:feather;DB_CLOSE_DELAY=-1") })

        var authApp: AuthorizerApi? = null
        var authorzationController: AuthorizationController? = null
    }

    @Test
    fun `Create session with valid mail and password returns CREATED (201)`() {
        val password = "secret"
        val port = 9001
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        val session = restConnection.loginRequest("harry77@testmail.de", password, port).asEmpty()
        assertEquals(HttpStatus.CREATED_201, session.status)
    }

    @Test
    fun `Create session method sets minisession to false in session database`() {
        val password = "secret"
        val port = 9010
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        val sessionResponse = restConnection.loginRequest("harry77@testmail.de", password, port).asObject<SessionAnswer>()

        assertNotNull(sessionResponse.body)

        val dbSession = authorzationController!!.getSession(sessionResponse.body.id)

        assertNotNull(dbSession)
        assertFalse(dbSession.miniSession)
    }

    @Test
    fun `Create session with valid username and password returns CREATED (201)`() {
        val password = "secret"
        val port = 9002
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        val session = restConnection.loginRequest("harry77@testmail.de", password, port).asEmpty()
        assertEquals(HttpStatus.CREATED_201, session.status)
    }

    @Test
    fun `Create session with invalid user returns UNAUTHORIZED (401)`() {
        val password = "secret"
        val port = 9003
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 78, password)
        val session = restConnection.loginRequest("harry77@testmail.de", password, port).asEmpty()
        assertEquals(HttpStatus.UNAUTHORIZED_401, session.status)
    }

    @Test
    fun `Create session with valid user but wrong password returns UNAUTHORIZED (401)`() {
        val password = "secret"
        val port = 9004
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        val session = restConnection.loginRequest("harry77@testmail.de", "blabla", port).asEmpty()
        assertEquals(HttpStatus.UNAUTHORIZED_401, session.status)
    }

    @Test
    fun `Get session with returns OK (200) for valid session`() {
        val password = "secret"
        val port = 9005
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        restConnection.loginRequest("harry77@testmail.de", password, port).asEmpty()
        val session = restConnection.get("http://127.0.0.1:$port/v1/session").asEmpty()
        assertEquals(HttpStatus.OK_200, session.status)
    }

    @Test
    fun `Get session with returns Not Found (404) for not existing session`() {
        val password = "secret"
        val port = 9006
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        val session = restConnection.get("http://127.0.0.1:$port/v1/session").asEmpty()
        assertEquals(HttpStatus.NOT_FOUND_404, session.status)
    }

    @Test
    fun `Create session delivers correct body`() {
        val password = "secret"
        val port = 9007
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        val session = restConnection.loginRequest("harry77@testmail.de", password, port).asObject<SessionAnswer>().body
        assert(session.id.toString().isNotBlank())
        assert(session.validUntil > Instant.now())
    }

    @Test
    fun `Get session delivers correct body`() {
        val password = "secret"
        val port = 9008
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        val firstSession = restConnection.loginRequest("harry77@testmail.de", password, port).asObject<SessionAnswer>().body
        val sessionResponse = restConnection.get("http://127.0.0.1:$port/v1/session").asObject<SessionAnswer>()
        val session = sessionResponse.body
        assertEquals(firstSession.id, session.id)
    }

    @Test
    fun `Delete session delivers 200 (OK)`() {
        val password = "secret"
        val port = 9009
        val restConnection = loadAuthorizerAndCreateTestConnection(port, 77, password)
        restConnection.loginRequest("harry77@testmail.de", password, port).asObject<SessionAnswer>().body
        val session = restConnection.delete("http://127.0.0.1:$port/v1/session").asObject<String>()
        assertEquals(HttpStatus.OK_200, session.status)
        assertEquals("http://127.0.0.1/login", session.body)
    }

    private fun UnirestInstance.loginRequest(username: String, password: String, port: Int): RequestBodyEntity =
        post("http://127.0.0.1:$port/v1/authorizer/credential").body(LoginRequest(username, password))

    fun loadAuthorizerAndCreateTestConnection(port: Int, userID: Int, password: String): UnirestInstance {
        val app = AppBuilder().createApp("test", "session-api-test", "http://127.0.0.1", emptySet(), port)
        val credentialMock = CredentialMock()
        val testUser = User(
            "$userID",
            "Harry$userID",
            "Harry$userID Test",
            "Harry$userID",
            "Test",
            "harry$userID@testmail.de",
            setOf(),
            Instant.now(),
            setOf(),
            setOf(Permission.USER),
            true
        )
        credentialMock.createUser(testUser, password)
        authorzationController = AuthorizationController(mutableSetOf(CredentialAuthorizer("credential", credentialMock, "http://127.0.0.1")), credentialMock::getUser, db, ActionController(db))
        authApp = AuthorizerApi(app, authorzationController!!, true)
        val restUser = UnirestInstance(
            Config().setObjectMapper(
                JacksonObjectMapper(
                    jacksonObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                        .registerModule(JavaTimeModule())
                )
            )
                .addDefaultHeader("Accept", "application/json")
        )
        return restUser
    }

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> =
        this.asObject(object : GenericType<B>() {})
}
