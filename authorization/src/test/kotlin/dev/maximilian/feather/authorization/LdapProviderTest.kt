/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.Group
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.Instant
import java.util.UUID
import kotlin.random.Random

fun getEnv(name: String, default: String? = null): String =
    System.getenv(name) ?: default ?: throw IllegalArgumentException("Environment variable $name not found")

class LdapProviderTest {
    companion object {
        private val credentialProvider = LdapProviderFactory(
            mutableMapOf(
                "ldap.host" to getEnv("LDAP_HOST", "127.0.0.1"),
                "ldap.port" to getEnv("LDAP_PORT", "389"),
                "ldap.tls" to "false",
                "ldap.bind.dn" to getEnv("LDAP_BIND_USER", "cn=admin,{{ baseDn }}"),
                "ldap.bind.password" to getEnv("LDAP_BIND_PASSWORD", "admin"),
                "ldap.unique_groups" to getEnv("LDAP_UNIQUE_GROUPS", "true"),
                "ldap.plain_text_passwords" to getEnv("PLAIN_TEXT_PASSWORDS", "false"),
                "ldap.dn.base" to getEnv("LDAP_BASE_DN", "dc=example,dc=org")
            )
        ).create()

        @AfterAll
        fun cleanup() {
            credentialProvider.close()
        }
    }

    @Test
    fun `Test get not existing user`() {
        assertNull(credentialProvider.getUser(UUID.randomUUID().toString()))
    }

    @Test
    fun `Test create simple user`() {
        val user = createTestUser()
        val createdUser = credentialProvider.createUser(user)
        val getUser = credentialProvider.getUser(createdUser.id)

        assertEquals(user, createdUser.copy(id = "", registeredSince = user.registeredSince))
        assertEquals(createdUser, getUser)
    }

    @Test
    fun `Test create user with permissions`() {
        val user = createTestUser().copy(permissions = setOf(Permission.INVITE, Permission.USER))
        val createdUser = credentialProvider.createUser(user)
        val getUser = credentialProvider.getUser(createdUser.id)

        assertEquals(user, createdUser.copy(id = "", registeredSince = user.registeredSince))
        assertEquals(createdUser, getUser)
    }

    @Test
    fun `Test create user with groups`() {
        val groups = (0..3).map { credentialProvider.createGroup(createTestGroup()) }
        val user = createTestUser().copy(
            groups = setOf(groups[0].id, groups[2].id, groups[3].id),
            ownedGroups = setOf(groups[1].id, groups[3].id)
        )
        val createdUser = credentialProvider.createUser(user)
        val getUser = credentialProvider.getUser(createdUser.id)

        assertEquals(
            user.copy(groups = groups.map { it.id }.toSet()),
            createdUser.copy(id = "", registeredSince = user.registeredSince)
        )
        assertEquals(createdUser, getUser)
    }

    @Test
    fun `Test delete user`() {
        val groups = (0..3).map { credentialProvider.createGroup(createTestGroup()) }
        val user = credentialProvider.createUser(
            createTestUser().copy(
                groups = setOf(groups[0].id, groups[2].id, groups[3].id),
                ownedGroups = setOf(groups[1].id, groups[3].id),
                permissions = setOf(Permission.USER)
            )
        )
        assertEquals(user, credentialProvider.getUser(user.id))

        credentialProvider.deleteUser(user)
        assertNull(credentialProvider.getUser(user.id))

        val recreatedUser = credentialProvider.createUser(
            user.copy(
                groups = emptySet(),
                ownedGroups = emptySet(),
                permissions = emptySet()
            )
        )
        assertEquals(recreatedUser, credentialProvider.getUser(recreatedUser.id))
    }

    @Test
    fun `Test update all user properties`() {
        val groups = (0..3).map { credentialProvider.createGroup(createTestGroup()) }
        val user = credentialProvider.createUser(
            createTestUser().copy(
                groups = setOf(groups[0].id, groups[2].id, groups[3].id),
                ownedGroups = setOf(groups[1].id, groups[3].id),
                permissions = setOf(Permission.ADMIN)
            )
        )
        val updatedUser = user.copy(
            displayName = null,
            firstname = "Updated Firstname ${UUID.randomUUID()}",
            mail = "updated+${UUID.randomUUID()}@maximilian.dev",
            surname = "Updated Surname ${UUID.randomUUID()}",
            username = "Updated Username ${UUID.randomUUID()}",
            permissions = setOf(Permission.USER, Permission.INVITE),
            groups = setOf(groups[0].id),
            ownedGroups = setOf(groups[0].id, groups[3].id)
        )
        val savedUpdatedUser = credentialProvider.updateUser(updatedUser)
        val getUser = credentialProvider.getUser(user.id)

        assertEquals(
            updatedUser.copy(
                groups = setOf(groups[0].id, groups[3].id)
            ),
            savedUpdatedUser
        )
        assertEquals(savedUpdatedUser, getUser)
    }

    @Test
    fun `Test update 'Nothing to update' user`() {
        val testGroups = (0 until 3).map { credentialProvider.createGroup(createTestGroup()) }
        val user = credentialProvider.createUser(
            createTestUser().copy(
                groups = setOf(testGroups[0].id, testGroups[2].id),
                ownedGroups = setOf(testGroups[2].id)
            )
        )

        val updatedUser = credentialProvider.updateUser(user)
        val getUser = credentialProvider.getUser(user.id)

        assertEquals(user, updatedUser)
        assertEquals(updatedUser, getUser)
    }

    @Test
    fun `Test get not existing group`() {
        assertNull(credentialProvider.getGroup(UUID.randomUUID().toString()))
    }

    @Test
    fun `Test create simple group`() {
        val group = createTestGroup()
        val createdGroup = credentialProvider.createGroup(group)
        val getGroup = credentialProvider.getGroup(createdGroup.id)

        assertEquals(group, createdGroup.copy(id = ""))
        assertEquals(createdGroup, getGroup)
    }

    @Test
    fun `Test create group with users`() {
        val users = (0..3).map { credentialProvider.createUser(createTestUser()) }
        val group = createTestGroup().copy(
            userMembers = setOf(users[0].id, users[2].id, users[3].id),
            owners = setOf(users[1].id, users[3].id)
        )
        val createdGroup = credentialProvider.createGroup(group)
        val getGroup = credentialProvider.getGroup(createdGroup.id)

        assertEquals(
            group.copy(userMembers = users.map { it.id }.toSet()),
            createdGroup.copy(id = "")
        )
        assertEquals(createdGroup, getGroup)
    }

    @Test
    fun `Test create group with child groups`() {
        val groups = (0..3).map { credentialProvider.createGroup(createTestGroup()) }
        val group = createTestGroup().copy(
            groupMembers = setOf(groups[0].id, groups[2].id, groups[3].id)
        )
        val createdGroup = credentialProvider.createGroup(group)
        val getGroup = credentialProvider.getGroup(createdGroup.id)

        assertEquals(group, createdGroup.copy(id = ""))
        assertEquals(createdGroup, getGroup)
    }

    @Test
    fun `Test create group with child and parent groups`() {
        val groups = (0..3).map { credentialProvider.createGroup(createTestGroup()) }
        val group = createTestGroup().copy(
            groupMembers = setOf(groups[0].id, groups[2].id, groups[3].id),
            parentGroups = setOf(groups[1].id)
        )
        val createdGroup = credentialProvider.createGroup(group)
        val getGroup = credentialProvider.getGroup(createdGroup.id)

        assertEquals(group, createdGroup.copy(id = ""))
        assertEquals(createdGroup, getGroup)
    }

    @Test
    fun `Test create group with owning and owned groups`() {
        val groups = (0..3).map { credentialProvider.createGroup(createTestGroup()) }
        val group = createTestGroup().copy(
            groupMembers = setOf(groups[0].id, groups[2].id),
            ownerGroups = setOf(groups[3].id),
            ownedGroups = setOf(groups[1].id)
        )
        val createdGroup = credentialProvider.createGroup(group)
        val getGroup = credentialProvider.getGroup(createdGroup.id)

        assertEquals(
            group.copy(
                groupMembers = group.groupMembers + group.ownerGroups,
                parentGroups = group.parentGroups + group.ownedGroups
            ),
            createdGroup.copy(id = "")
        )
        assertEquals(createdGroup, getGroup)
    }

    @Test
    fun `Test delete group`() {
        val group = credentialProvider.createGroup(createTestGroup())
        assertEquals(group, credentialProvider.getGroup(group.id))

        credentialProvider.deleteGroup(group)
        assertNull(credentialProvider.getGroup(group.id))
    }

    @Test
    fun `Test authenticate not existing user`() {
        assertNull(credentialProvider.authenticateUser(UUID.randomUUID().toString(), UUID.randomUUID().toString()))
    }

    @Test
    fun `Test authenticate user with false password`() {
        val user = credentialProvider.createUser(createTestUser(), UUID.randomUUID().toString())
        assertNull(credentialProvider.authenticateUser(user.username, UUID.randomUUID().toString()))
    }

    @Test
    fun `Test authenticate user`() {
        val password = UUID.randomUUID().toString()
        val user = credentialProvider.createUser(createTestUser(), password)
        assertEquals(user, credentialProvider.authenticateUser(user.username, password))
    }

    @Test
    fun `Test authenticate user by mail`() {
        val password = UUID.randomUUID().toString()
        val user = credentialProvider.createUser(createTestUser(), password)
        assertEquals(user, credentialProvider.authenticateUserByMail(user.mail, password))
    }

    @Test
    fun `Test update password for non existing user`() {
        assertFalse(credentialProvider.updateUserPassword(createTestUser(), UUID.randomUUID().toString()))
    }

    @Test
    fun `Test update user password`() {
        val user = credentialProvider.createUser(createTestUser())
        val password = UUID.randomUUID().toString()

        // Test password set
        assertTrue(credentialProvider.updateUserPassword(user, password))
        assertEquals(user, credentialProvider.authenticateUserByMail(user.mail, password))

        // Test password update
        assertTrue(credentialProvider.updateUserPassword(user, password))
        assertEquals(user, credentialProvider.authenticateUserByMail(user.mail, password))
    }

    @Test
    fun `Test update all group properties`() {
        val users = (0..3).map { credentialProvider.createUser(createTestUser()) }
        val group = credentialProvider.createGroup(
            createTestGroup().copy(
                userMembers = setOf(users[0].id, users[2].id, users[3].id),
                owners = setOf(users[1].id, users[3].id)
            )
        )
        /* update all properties except for groupMembers and parentGroups
         * (there is an extra test 'Test modify members of group' for that) */
        val updatedGroup = group.copy(
            name = "Updated ${UUID.randomUUID()}",
            description = null,
            userMembers = setOf(users[0].id),
            owners = setOf(users[1].id)
        )
        val savedUpdatedGroup = credentialProvider.updateGroup(updatedGroup)
        val getGroup = credentialProvider.getGroup(group.id)

        assertEquals(updatedGroup.copy(userMembers = setOf(users[0].id, users[1].id)), savedUpdatedGroup)
        assertEquals(savedUpdatedGroup, getGroup)
    }

    @Test
    fun `Test update 'Nothing to update' group`() {
        val testUser = (0 until 3).map { credentialProvider.createUser(createTestUser()) }
        val group = credentialProvider.createGroup(
            createTestGroup().copy(
                userMembers = setOf(testUser[0].id, testUser[2].id),
                owners = setOf(testUser[2].id)
            )
        )
        val updatedGroup = credentialProvider.updateGroup(group)
        val getGroup = credentialProvider.getGroup(group.id)

        assertEquals(group, updatedGroup)
        assertEquals(updatedGroup, getGroup)
    }

    @Test
    fun `Test modify members of group`() {
        val users = (0..3).map { credentialProvider.createUser(createTestUser()) }
        val groups = (0..3).map { credentialProvider.createGroup(createTestGroup()) }
        val group = createTestGroup().copy(
            userMembers = setOf(users[0].id, users[2].id, users[3].id),
            owners = setOf(users[1].id, users[3].id),
            groupMembers = setOf(groups[0].id, groups[2].id, groups[3].id)
        )
        val createdGroup = credentialProvider.createGroup(group)

        // update only user members and owners
        val updatedGroup = createdGroup.copy(
            name = "Updated ${UUID.randomUUID()}",
            description = null,
            userMembers = setOf(users[0].id),
            owners = setOf(users[1].id)
        )
        val savedUpdatedGroup = credentialProvider.updateGroup(updatedGroup)
        val getGroup = credentialProvider.getGroup(createdGroup.id)

        assertEquals(updatedGroup.copy(userMembers = setOf(users[0].id, users[1].id)), savedUpdatedGroup)
        assertEquals(savedUpdatedGroup, getGroup)

        // update only child and parent groups
        val updatedGroup2 = updatedGroup.copy(
            name = "Updated ${UUID.randomUUID()}",
            description = null,
            groupMembers = setOf(groups[2].id),
            parentGroups = setOf(groups[1].id)
        )
        val savedUpdatedGroup2 = credentialProvider.updateGroup(updatedGroup2)
        val getGroup2 = credentialProvider.getGroup(createdGroup.id)

        assertEquals(updatedGroup2.copy(userMembers = setOf(users[0].id, users[1].id)), savedUpdatedGroup2)
        assertEquals(savedUpdatedGroup2, getGroup2)
    }

    @Test
    fun `Test get all users and groups`() {
        val users = (0 until 10).map {
            credentialProvider.createUser(
                createTestUser().copy(
                    permissions = Permission.values().take(Random.nextInt(2)).toSet()
                )
            )
        }
        val parentGroups = (0 until 5).map {
            credentialProvider.createGroup(
                createTestGroup().copy(
                    owners = users.shuffled().take(3).map { it.id }.toSet(),
                    userMembers = users.shuffled().take(3).map { it.id }.toSet()
                )
            )
        }

        val permittedIds = (
            users.map { it.id } +
                parentGroups.map { it.id }
            ).toSet()
        val usersWithGroups = users.map { credentialProvider.getUser(it.id) }
        val allUsersAndGroups = credentialProvider.getUsersAndGroups()

        assertEquals(usersWithGroups.toSet(), allUsersAndGroups.first.filter { permittedIds.contains(it.id) }.toSet())
        assertEquals(parentGroups.toSet(), allUsersAndGroups.second.filter { permittedIds.contains(it.id) }.toSet())

        val childGroups = (0 until 5).map {
            credentialProvider.createGroup(
                createTestGroup().copy(
                    owners = users.shuffled().take(3).map { it.id }.toSet(),
                    userMembers = users.shuffled().take(3).map { it.id }.toSet(),
                    parentGroups = parentGroups.shuffled().take(1).map { it.id }.toSet()
                )
            )
        }

        val parentGroups2 = parentGroups.map { credentialProvider.getGroup(it.id) }
        val permittedIds2 = (
            users.map { it.id } +
                parentGroups.map { it.id } +
                childGroups.map { it.id }
            ).toSet()
        val usersWithGroups2 = users.map { credentialProvider.getUser(it.id) }
        val allUsersAndGroups2 = credentialProvider.getUsersAndGroups()

        assertEquals(usersWithGroups2.toSet(), allUsersAndGroups2.first.filter { permittedIds2.contains(it.id) }.toSet())
        assertEquals((parentGroups2 + childGroups).toSet(), allUsersAndGroups2.second.filter { permittedIds2.contains(it.id) }.toSet())
    }

    @Test
    fun `Test implicit membership`() {
        val users = (0..3).map { credentialProvider.createUser(createTestUser()) }
        val parentGroup = createTestGroup().copy(
            userMembers = setOf(users[0].id),
            owners = setOf(users[1].id)
        )
        val createdParentGroup = credentialProvider.createGroup(parentGroup)

        val childGroup = createTestGroup().copy(
            userMembers = setOf(users[2].id),
            owners = setOf(users[3].id),
            parentGroups = setOf(createdParentGroup.id)
        )
        val createdChildGroup = credentialProvider.createGroup(childGroup)

        val getParentGroup = credentialProvider.getGroup(createdParentGroup.id)
        val getChildGroup = credentialProvider.getGroup(createdChildGroup.id)
        assertNotNull(getParentGroup)
        assertNotNull(getChildGroup)

        assertFalse(getParentGroup?.hasOwnerRights(users[0].id, credentialProvider) ?: true)
        assertTrue(getParentGroup?.hasOwnerRights(users[1].id, credentialProvider) ?: false)
        assertFalse(getParentGroup?.hasOwnerRights(users[3].id, credentialProvider) ?: true)
        assertFalse(getChildGroup?.hasOwnerRights(users[1].id, credentialProvider) ?: true)
        assertFalse(getChildGroup?.hasOwnerRights(users[2].id, credentialProvider) ?: true)
        assertTrue(getChildGroup?.hasOwnerRights(users[3].id, credentialProvider) ?: false)

        assertTrue(getChildGroup?.isParentGroup(createdParentGroup.id, credentialProvider) ?: false)
        assertFalse(getParentGroup?.isParentGroup(createdChildGroup.id, credentialProvider) ?: true)

        assertTrue(getParentGroup?.isGroupMemberRecursive(createdChildGroup.id, credentialProvider) ?: false)
        assertFalse(getChildGroup?.isGroupMemberRecursive(createdParentGroup.id, credentialProvider) ?: true)

        assertTrue(getParentGroup?.isUserMemberRecursive(users[0].id, credentialProvider) ?: false)
        assertTrue(getParentGroup?.isUserMemberRecursive(users[2].id, credentialProvider) ?: false)
        assertFalse(getChildGroup?.isUserMemberRecursive(users[0].id, credentialProvider) ?: true)
        assertTrue(getChildGroup?.isUserMemberRecursive(users[2].id, credentialProvider) ?: false)
    }

    @Test
    fun `Test implicit ownership`() {
        val users = (0..1).map { credentialProvider.createUser(createTestUser()) }
        val owningGroup = createTestGroup().copy(
            userMembers = setOf(users[0].id)
        )
        val createdOwningGroup = credentialProvider.createGroup(owningGroup)

        val ownedGroup = createTestGroup().copy(
            userMembers = setOf(users[1].id),
            ownerGroups = setOf(createdOwningGroup.id)
        )
        val createdOwnedGroup = credentialProvider.createGroup(ownedGroup)

        val getOwningGroup = credentialProvider.getGroup(createdOwningGroup.id)
        val getOwnedGroup = credentialProvider.getGroup(createdOwnedGroup.id)
        assertNotNull(getOwningGroup)
        assertNotNull(getOwnedGroup)

        assertTrue(getOwningGroup?.isUserMemberRecursive(users[0].id, credentialProvider) ?: false)
        assertFalse(getOwningGroup?.isUserMemberRecursive(users[1].id, credentialProvider) ?: true)
        assertFalse(getOwnedGroup?.userMembers?.contains(users[0].id) ?: true)
        assertTrue(getOwnedGroup?.isUserMemberRecursive(users[0].id, credentialProvider) ?: false)
        assertTrue(getOwnedGroup?.isUserMemberRecursive(users[1].id, credentialProvider) ?: false)

        assertFalse(getOwningGroup?.hasOwnerRights(users[0].id, credentialProvider) ?: true)
        assertFalse(getOwningGroup?.hasOwnerRights(users[1].id, credentialProvider) ?: true)
        assertTrue(getOwnedGroup?.hasOwnerRights(users[0].id, credentialProvider) ?: false)
        assertFalse(getOwnedGroup?.hasOwnerRights(users[1].id, credentialProvider) ?: true)
    }

    private fun createTestUser(): User = User(
        id = "",
        username = "Testuser${UUID.randomUUID()}",
        surname = "Surname ${UUID.randomUUID()}",
        mail = "test+${UUID.randomUUID()}@maximilian.dev",
        registeredSince = Instant.ofEpochMilli(0),
        ownedGroups = emptySet(),
        groups = emptySet(),
        permissions = emptySet(),
        firstname = "Firstname ${UUID.randomUUID()}",
        displayName = "Displayname ${UUID.randomUUID()}"
    )

    private fun createTestGroup(): Group = Group(
        id = "",
        description = "Description ${UUID.randomUUID()}",
        userMembers = emptySet(),
        name = "Name ${UUID.randomUUID()}",
        dnParent = null,
        parentGroups = emptySet(),
        groupMembers = emptySet(),
        owners = emptySet(),
        ownerGroups = emptySet(),
        ownedGroups = emptySet()
    )
}
