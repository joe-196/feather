/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.authorization.internals.RedisCacheAdapter
import redis.clients.jedis.JedisPool

class CachedCredentialProvider(pool: JedisPool, private val credentialProvider: ICredentialProvider) : ICredentialProvider {
    private val cacheAdapter = RedisCacheAdapter(pool, credentialProvider)

    override fun getUser(id: String): User? {
        return cacheAdapter.getUser(id)
    }

    override fun getUsers(): Collection<User> {
        return cacheAdapter.getUsers()
    }

    override fun createUser(user: User, password: String?): User {
        val userReturned = credentialProvider.createUser(user, password)
        cacheAdapter.refreshCache()
        return userReturned
    }

    override fun deleteUser(user: User) {
        credentialProvider.deleteUser(user)
        cacheAdapter.refreshCache()
    }

    override fun updateUser(user: User): User? {
        val userReturned = credentialProvider.updateUser(user)
        cacheAdapter.refreshCache()
        return userReturned
    }

    override fun authenticateUser(username: String, password: String): User? {
        return credentialProvider.authenticateUser(username, password)
    }

    override fun authenticateUserByMail(mail: String, password: String): User? {
        return credentialProvider.authenticateUserByMail(mail, password)
    }

    override fun updateUserPassword(user: User, password: String): Boolean {
        return credentialProvider.updateUserPassword(user, password)
    }

    override fun getGroup(id: String): Group? {
        return cacheAdapter.getGroup(id)
    }

    override fun getGroupByName(groupname: String): Group? {
        return cacheAdapter.getGroupByName(groupname)
    }

    override fun getGroups(): Collection<Group> {
        return cacheAdapter.getGroups()
    }

    override fun createGroup(group: Group): Group {
        val groupReturned = credentialProvider.createGroup(group)
        cacheAdapter.refreshCache()
        return groupReturned
    }

    override fun deleteGroup(group: Group) {
        credentialProvider.deleteGroup(group)
        cacheAdapter.refreshCache()
    }

    override fun updateGroup(group: Group): Group? {
        val groupReturned = credentialProvider.updateGroup(group)
        cacheAdapter.refreshCache()
        return groupReturned
    }

    override fun getUsersAndGroups(): Pair<Collection<User>, Collection<Group>> {
        val groups = cacheAdapter.getGroups()
        val users = cacheAdapter.getUsers()
        return Pair(users, groups)
    }

    override fun close() {
        credentialProvider.close()
    }

    override fun getUserByMail(mail: String): User? = cacheAdapter.getUserByMail(mail)
    override fun getUserByUsername(username: String): User? = cacheAdapter.getUserByUsername(username)
}
