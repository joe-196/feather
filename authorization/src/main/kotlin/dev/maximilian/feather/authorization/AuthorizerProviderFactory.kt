/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.maximilian.feather.IAuthorizer
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.authorization.internals.oidc.OidcAutoDiscoveryClient
import dev.maximilian.feather.authorization.internals.oidc.OidcProviderType
import mu.KLogging

class AuthorizerProviderFactory(private val propertyMap: MutableMap<String, String>) {
    companion object : KLogging()

    private val objectMapper = jacksonObjectMapper()
    private val oidcProviderList = propertyMap.getOrPut("authorizer.oidc") { AuthorizerConstants.OIDC_PROVIDER_LIST }
    private val credentialAuthorizerEnabled =
        propertyMap.getOrPut("authorizer.credential") { AuthorizerConstants.CREDENTIAL_AUTHORIZER_ENABLED }

    fun create(baseUrl: String, credentialProvider: ICredentialProvider): Set<IAuthorizer> {
        val validatedProviderList =
            kotlin.runCatching { objectMapper.readValue<Set<String>>(oidcProviderList) }.getOrNull()
        val credentialAuthorizer = if (credentialAuthorizerEnabled == "true") CredentialAuthorizer(
            "credential",
            credentialProvider,
            baseUrl
        ) else null

        val authorizerList = mutableListOf<IAuthorizer>()

        if (credentialAuthorizer != null) {
            authorizerList.add(credentialAuthorizer)
        }

        if (validatedProviderList == null) {
            logger.error { "OIDC Provider list malformed, did'nt load any" }
        } else {
            authorizerList.addAll(
                validatedProviderList.mapNotNull {
                    kotlin.runCatching { createOIDCProvider(it, baseUrl, credentialProvider) }
                        .onFailure { t -> logger.error(t) { "Disabling OIDC Provider $it because of malformed config" } }
                        .getOrNull()
                }
            )
        }

        return authorizerList.toSet()
    }

    private fun createOIDCProvider(id: String, baseUrl: String, credentialProvider: ICredentialProvider): IAuthorizer {
        val baseProperty = "authorizer.oidc.$id"

        val name = propertyMap.getOrPut("$baseProperty.name") { AuthorizerConstants.NAME }
        val color = propertyMap.getOrPut("$baseProperty.color") { AuthorizerConstants.COLOR }
        val iconUrl = propertyMap.getOrPut("$baseProperty.url.icon") { AuthorizerConstants.ICON_URL }

        val apiKey = propertyMap.getOrPut("$baseProperty.apiKey") { AuthorizerConstants.OIDC_KEY }
        val apiSecret = propertyMap.getOrPut("$baseProperty.apiSecret") { AuthorizerConstants.OIDC_SECRET }
        val apiProvider = propertyMap.getOrPut("$baseProperty.apiProvider") { AuthorizerConstants.OIDC_PROVIDER }

        require(name.isNotBlank()) { "A name for an oidc provider is required" }
        require(color.isNotBlank()) { "A color for an oidc provider is required" }
        require(iconUrl.isNotBlank()) { "An iconUrl for an oidc provider is required" }
        require(apiKey.isNotBlank()) { "An apiKey for an oidc provider is required" }
        require(apiSecret.isNotBlank()) { "An apiSecret for an oidc provider is required" }

        val providerType: OidcProviderType? =
            OidcProviderType.values().firstOrNull { it.name.equals(apiProvider, true) }
        require(providerType != null) {
            "OIDC provider type not found, please specify one of ${
            OidcProviderType.values()
                .joinToString(",", "[", "]")
            }"
        }

        return when (providerType) {
            OidcProviderType.AUTO_DISCOVER -> createAutodiscoverOIDCProvider(
                id,
                baseUrl,
                credentialProvider,
                name,
                color,
                iconUrl,
                apiKey,
                apiSecret
            )
        }
    }

    private fun createAutodiscoverOIDCProvider(
        id: String,
        baseUrl: String,
        credentialProvider: ICredentialProvider,
        name: String,
        color: String,
        iconUrl: String,
        apiKey: String,
        apiSecret: String
    ): OidcAutoDiscoveryClient {
        val baseProperty = "authorizer.oidc.$id"

        val discoverUrl =
            propertyMap.getOrPut("$baseProperty.url.discover") { AuthorizerConstants.AUTO_DISCOVER_BASE_URL }
        require(discoverUrl.isNotBlank()) { "A discover url for an autodiscover oidc authorizer is required" }

        return OidcAutoDiscoveryClient(
            id, name, color, iconUrl, apiKey, apiSecret, discoverUrl, baseUrl, credentialProvider
        )
    }
}

object AuthorizerConstants {
    const val OIDC_PROVIDER_LIST = "[]"
    const val CREDENTIAL_AUTHORIZER_ENABLED = "true"
    const val NAME = "Example OIDC provider"
    const val ICON_URL = "account-key"
    const val COLOR = "rgb(0, 169, 224)"
    const val OIDC_KEY = "feather"
    const val OIDC_SECRET = "feather-secret"
    const val OIDC_PROVIDER = "AUTO_DISCOVER"
    const val AUTO_DISCOVER_BASE_URL = ""
}
