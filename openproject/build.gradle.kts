/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

plugins {
    kotlin("plugin.serialization") version kotlinVersion
}

dependencies {
    // Html parsing
    implementation("org.jsoup", "jsoup", jsoupVersion)

    // Ktor client
    implementation("io.ktor", "ktor-client-core", ktorVersion)
    implementation("io.ktor", "ktor-client-cio", ktorVersion)
    implementation("io.ktor", "ktor-client-serialization", ktorVersion)
    implementation("io.ktor", "ktor-client-auth", ktorVersion)
    implementation("io.ktor", "ktor-client-logging", ktorVersion)

    // Ktor server, only used for testing
    testImplementation("io.ktor", "ktor-server-core", ktorVersion)
    testImplementation("io.ktor", "ktor-server-netty", ktorVersion)
}

kotlin {
    explicitApi()
}
