/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.IdLazyEntity
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.openproject.OpenProjectUser
import dev.maximilian.feather.openproject.retryWithExponentialBackoff
import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.features.ClientRequestException
import io.ktor.client.features.RedirectResponseException
import io.ktor.client.request.cookie
import io.ktor.client.request.delete
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.patch
import io.ktor.client.request.post
import io.ktor.http.HttpStatusCode
import io.ktor.http.parametersOf
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.Duration

public interface IOpenProjectGroupApi {
    // Group Create, Get, Delete
    public suspend fun getGroups(): List<OpenProjectGroup>
    public suspend fun getGroup(id: Int): OpenProjectGroup?
    public suspend fun getGroupByName(name: String): OpenProjectGroup?
    public suspend fun createGroup(group: OpenProjectGroup): OpenProjectGroup
    public suspend fun deleteGroup(group: OpenProjectGroup)

    // Group Member Add/Remove
    public suspend fun addGroupMember(group: OpenProjectGroup, user: OpenProjectUser): OpenProjectGroup
    public suspend fun removeGroupMember(group: OpenProjectGroup, user: OpenProjectUser): OpenProjectGroup

    // LDAP group synchronisation, requires OpenProject Enterprise license
    // It is not checked whether the license exists
    public suspend fun createLdapGroupSynchronisation(
        group: OpenProjectGroup,
        ldapDn: String,
        authSourceId: Int
    )
}

internal class GroupApi(
    private val apiClient: HttpClient,
    private val webpageClient: HttpClient,
    private val baseUrl: String,
    private val userApi: IOpenProjectUserApi
) : IOpenProjectGroupApi {
    companion object {
        internal fun groupUrl(baseUrl: String, id: Int? = null) = "$baseUrl/api/v3/groups" + (id?.let { "/$it" } ?: "")
    }

    override suspend fun getGroups(): List<OpenProjectGroup> =
        apiClient.getAllPages<GroupAnswer>(groupUrl(baseUrl)).toOpenProjectEntity()

    override suspend fun getGroup(id: Int): OpenProjectGroup? =
        apiClient.getOrNull<GroupAnswer>(groupUrl(baseUrl, id))?.toOpenProjectEntity(mutableMapOf())

    override suspend fun getGroupByName(name: String): OpenProjectGroup? =
        getGroups().firstOrNull { it.name == name }

    override suspend fun createGroup(group: OpenProjectGroup): OpenProjectGroup =
        try {
            apiClient.post<GroupAnswer>(groupUrl(baseUrl)) {
                body = CreateGroupRequest(group.name)
            }.toOpenProjectEntity(mutableMapOf())
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.UnprocessableEntity) {
                throw IllegalArgumentException(e.response.receive<OpenProjectErrorAnswer>().message)
            } else {
                throw e
            }
        }

    override suspend fun deleteGroup(group: OpenProjectGroup) {
        apiClient.delete<Unit>(groupUrl(baseUrl, group.id)) {
            // Workaround for https://youtrack.jetbrains.com/issue/KTOR-1407
            // Also why OpenProject expects a content-type here?
            body = ""
        }

        val waitingFunction: suspend() -> Unit = {
            require(getGroup(group.id) == null) { "Group deletion exceeds time limit" }
        }

        waitingFunction.retryWithExponentialBackoff(6, Duration.ofSeconds(180))
    }

    override suspend fun addGroupMember(group: OpenProjectGroup, user: OpenProjectUser): OpenProjectGroup {
        val updatedGroup = getGroup(group.id)

        requireNotNull(updatedGroup) { "Group with id ${group.id} not found, cannot updated" }

        return apiClient.patch<GroupAnswer>(groupUrl(baseUrl, updatedGroup.id)) {
            body = UpdateGroupMembersRequest(
                links = GroupAnswerLinks(
                    members = (updatedGroup.members.map { it.id } + user.id).toSet()
                        .map { HrefAnswer(UserApi.userUrl(baseUrl, it)) }
                )
            )
        }.toOpenProjectEntity(group.members.associateBy { it.id }.toMutableMap())
    }

    override suspend fun removeGroupMember(group: OpenProjectGroup, user: OpenProjectUser): OpenProjectGroup {
        val updatedGroup = getGroup(group.id)

        requireNotNull(updatedGroup) { "Group with id ${group.id} not found, cannot updated" }

        return apiClient.patch<GroupAnswer>(groupUrl(baseUrl, updatedGroup.id)) {
            body = UpdateGroupMembersRequest(
                links = GroupAnswerLinks(
                    members = (updatedGroup.members.map { it.id } - user.id).toSet()
                        .map { HrefAnswer(UserApi.userUrl(baseUrl, it)) }
                )
            )
        }.toOpenProjectEntity(group.members.associateBy { it.id }.toMutableMap())
    }

    override suspend fun createLdapGroupSynchronisation(group: OpenProjectGroup, ldapDn: String, authSourceId: Int) {
        val (cookie, csrfToken) = webpageClient.getCookieAndCsrf("$baseUrl/ldap_groups/synchronized_groups/new")

        try {
            webpageClient.submitForm<String>(
                "$baseUrl/ldap_groups/synchronized_groups",
                parametersOf(
                    "synchronized_group[auth_source_id]" to listOf("$authSourceId"),
                    "synchronized_group[dn]" to listOf(ldapDn),
                    "synchronized_group[group_id]" to listOf(group.id.toString()),
                    "authenticity_token" to listOf(csrfToken)
                )
            ) { cookie("_open_project_session", cookie) }

            throw IllegalStateException(
                "Cannot create ldap group synchronisation for OpenProject group ${group.id} and ldap dn $ldapDn. " +
                    "Unknown error (no redirect)"
            )
        } catch (e: RedirectResponseException) {
            // consume
            return
        }
    }

    private fun GroupAnswer.toOpenProjectEntity(lazyMemberMap: MutableMap<Int, IdLazyEntity<OpenProjectUser?>>) = OpenProjectGroup(
        id = id,
        name = name,
        members = links
            .members
            .mapNotNull { it.href?.idFromURL() }
            .map { lazyMemberMap.getOrPut(it) { IdLazyEntity(it) { runBlocking { userApi.getUser(it) } } } }
    )

    private fun Iterable<GroupAnswer>.toOpenProjectEntity(): List<OpenProjectGroup> {
        val lazyMap = mutableMapOf<Int, IdLazyEntity<OpenProjectUser?>>()
        return map { it.toOpenProjectEntity(lazyMap) }
    }
}

@Serializable
private data class CreateGroupRequest(
    val name: String,
)

@Serializable
private data class UpdateGroupMembersRequest(
    @SerialName("_links")
    val links: GroupAnswerLinks
)

@Serializable
private data class GroupAnswer(
    val id: Int,
    val name: String,
    @SerialName("_links")
    val links: GroupAnswerLinks
)

@Serializable
private data class GroupAnswerLinks(
    val members: List<HrefAnswer>
)
