/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject

import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.features.HttpClientFeature
import io.ktor.client.features.HttpSend
import io.ktor.client.features.get
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.http.HttpStatusCode
import io.ktor.util.AttributeKey
import kotlinx.coroutines.delay

public class RetryOnTemporarilyServerError(
    private val retryCount: Int,
    private val delay: Long
) {
    public class Config {
        public var retryCount: Int = 3
            set(value) {
                require(value >= 0) { "retryCount needs to be positive or 0" }
                field = value
            }
    }

    public companion object : HttpClientFeature<Config, RetryOnTemporarilyServerError> {
        override val key: AttributeKey<RetryOnTemporarilyServerError> = AttributeKey("RetryOnTemporarilyServerError")

        override fun prepare(block: Config.() -> Unit): RetryOnTemporarilyServerError {
            val config = Config().apply(block)
            return RetryOnTemporarilyServerError(config.retryCount, 100)
        }

        override fun install(feature: RetryOnTemporarilyServerError, scope: HttpClient) {
            scope[HttpSend].intercept { origin, context ->
                var call = origin
                val retry = origin.attributes.computeIfAbsent(key) { feature }

                if (retry.retryCount > 0 && (call.response.status == HttpStatusCode.BadGateway || call.response.status == HttpStatusCode.ServiceUnavailable || call.response.status == HttpStatusCode.GatewayTimeout)) {
                    delay(retry.delay)

                    val requestBuilder = HttpRequestBuilder().takeFrom(context).apply {
                        attributes.put(key, RetryOnTemporarilyServerError(retry.retryCount - 1, retry.delay * 2))
                    }

                    call = execute(requestBuilder)
                }

                call
            }
        }
    }
}

public fun HttpClientConfig<*>.retryOnTemporarilyServerError(block: RetryOnTemporarilyServerError.Config.() -> Unit) {
    install(RetryOnTemporarilyServerError, block)
}

public fun HttpClientConfig<*>.retryOnTemporarilyServerError(): Unit = retryOnTemporarilyServerError { retryCount = 3 }
