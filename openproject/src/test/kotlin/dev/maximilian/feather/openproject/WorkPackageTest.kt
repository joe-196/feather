/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.IOpenProjectMembershipApi
import dev.maximilian.feather.openproject.api.IOpenProjectProjectApi
import dev.maximilian.feather.openproject.api.IOpenProjectRoleApi
import dev.maximilian.feather.openproject.api.IOpenProjectStatusApi
import dev.maximilian.feather.openproject.api.IOpenProjectUserApi
import dev.maximilian.feather.openproject.api.IOpenProjectWorkPackageApi
import dev.maximilian.feather.openproject.api.WorkPackageApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.assertDoesNotThrow
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkPackageTest {
    private val api: IOpenProjectWorkPackageApi = TestUtil.openProject
    private val userApi: IOpenProjectUserApi = TestUtil.openProject
    private val projectApi: IOpenProjectProjectApi = TestUtil.openProject
    private val statusApi: IOpenProjectStatusApi = TestUtil.openProject
    private val membershipApi: IOpenProjectMembershipApi = TestUtil.openProject
    private val roleApi: IOpenProjectRoleApi = TestUtil.openProject

    @Test
    fun `WorkPackages by user returns 33 packages where admin is assignee`() {
        runBlocking {
            val op = TestUtil.openProject as OpenProject
            val user = userApi.createUser(generateTestUser(), "test123456")
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val role = roleApi.getRoles().first { it.name == "Project admin" }
            membershipApi.createMembership(OpenProjectMembership(0, project, user, listOf(role)))
            val workPackage = api.getWorkPackagesOfProject(project, statusApi.getStatus(), false).minByOrNull { it.id }!!
            WorkPackageApi(op.apiClient, op.baseUrl).assignUserToWorkPackage(workPackage, user)
            val packages = api.getWorkPackagesOfUser(user, emptyList()).toSet()

            assertEquals(setOf(workPackage.id), packages.map { it.id }.toSet())
        }
    }

    @Test
    fun `WorkPackages Api ignores OpenProject bug on user with no memberships`() {
        val user = runBlocking { userApi.createUser(generateTestUser(), "test123456") }
        val workPackages = assertDoesNotThrow { runBlocking { api.getWorkPackagesOfUser(user, emptyList()) } }

        assertEquals(emptyList(), workPackages)
    }

    @Test
    fun `WorkPackages by project returns 9 open packages for demo project`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val workPackages = api.getWorkPackagesOfProject(project, statusApi.getStatus(), false)

            assertEquals(9, workPackages.size, "Expected 9 open work packages to be part of demo project")
        }
    }

    @Test
    fun `WorkPackages by project returns 10 open and closed packages for demo project`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val workPackages = api.getWorkPackagesOfProject(project, statusApi.getStatus(), true)

            assertEquals(10, workPackages.size, "Expected 10 open and closed work packages to be part of demo project")
        }
    }
}
