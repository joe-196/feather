/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.delete
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.put
import io.javalin.http.ConflictResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import mu.KLogging

internal class GroupApi(app: Javalin, private val credentialProvider: ICredentialProvider) {
    companion object : KLogging()
    init {
        app.routes {
            path("groups") {
                get(::getAllGroups)
                path(":id") {
                    get(::getGroup)
                    put("/member", ::addMemberToGroup)
                    delete("/member/:memberId", ::removeMemberFromGroup)
                    put("/owner", ::addOwnerToGroup)
                    delete("/owner/:memberId", ::removeOwnerFromGroup)
                    put("/parents", ::addParentsToGroup)
                    delete("/parents/:memberId", ::removeParentFromGroup)
                    put("/children", ::addChildrenToGroup)
                    delete("/children/:memberId", ::removeChildFromGroup)
                    put("/ownergroups", ::addOwnerGroupsToGroup)
                    delete("/ownergroups/:memberId", ::removeOwnerGroupFromGroup)
                    put("/ownedgroups", ::addOwnedGroupsToGroup)
                    delete("/ownedgroups/:memberId", ::removeOwnedGroupFromGroup)
                }
            }
        }
    }

    private fun getAllGroups(ctx: Context) {
        ctx.session()
        ctx.status(200)
        ctx.json(credentialProvider.getGroups())
    }

    private fun getGroup(ctx: Context) {
        ctx.session()
        val id = ctx.pathParam("id")
        val group = credentialProvider.getGroup(id) ?: throw NotFoundResponse("Group $id not found")

        ctx.json(group)
        ctx.status(200)
    }

    private fun Group.isAdminOrOwnerOfDnParent(user: User, iCredentialProvider: ICredentialProvider): Boolean = user.permissions.contains(Permission.ADMIN) ||
        (dnParent?.let { credentialProvider.getGroup(it) }?.hasOwnerRights(user.id, iCredentialProvider) ?: false)

    private fun Group.isAdminOrGroupOwner(user: User): Boolean = user.permissions.contains(Permission.ADMIN) ||
        hasOwnerRights(user.id, credentialProvider)

    private fun removeMemberFromGroup(ctx: Context) {
        val (user, group, removeId) = prepareRemoveGroupMemberOwner(ctx)

        if (group.owners.contains(removeId)) {
            if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
                throw ForbiddenResponse()
            }
        } else if (group.userMembers.contains(removeId)) {
            if (!group.isAdminOrGroupOwner(user)) {
                throw ForbiddenResponse()
            }
        } else {
            throw NotFoundResponse()
        }

        credentialProvider.updateGroup(group.copy(userMembers = group.userMembers - removeId, owners = group.owners - removeId))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun removeOwnerFromGroup(ctx: Context) {
        val (user, group, removeId) = prepareRemoveGroupMemberOwner(ctx)

        if (!group.owners.contains(removeId)) {
            throw NotFoundResponse()
        }
        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        }

        credentialProvider.updateGroup(group.copy(owners = group.owners - removeId))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun prepareRemoveGroupMemberOwner(ctx: Context): Triple<User, Group, String> {
        val session = ctx.session()
        val user = session.user
        val groupId = ctx.pathParam("id")
        val group = credentialProvider.getGroup(groupId) ?: throw NotFoundResponse("Group $groupId not found")
        val removeId = ctx.pathParam("memberId")

        return Triple(user, group, removeId)
    }

    private fun addMemberToGroup(ctx: Context) {
        val (user, group, toAdd) = prepareAddGroupMemberOwner(ctx)

        if (!group.isAdminOrGroupOwner(user)) {
            throw ForbiddenResponse()
        }

        credentialProvider.updateGroup(group.copy(userMembers = group.userMembers + toAdd))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun addOwnerToGroup(ctx: Context) {
        val (user, group, toAdd) = prepareAddGroupMemberOwner(ctx)

        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        }

        credentialProvider.updateGroup(group.copy(userMembers = group.userMembers + toAdd, owners = group.owners + toAdd))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun prepareAddGroupMemberOwner(ctx: Context): Triple<User, Group, Set<String>> {
        val session = ctx.session()
        val user = session.user
        val groupId = ctx.pathParam("id")
        val toAdd = ctx.body<Set<String>>()
        val group = credentialProvider.getGroup(groupId) ?: throw NotFoundResponse("Group $groupId not found")

        return Triple(user, group, toAdd)
    }

    private fun addParentsToGroup(ctx: Context) {
        val (user, group, toAdd) = prepareAddRelativeGroups(ctx)

        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        } else if (toAdd.any { group.id == it }) {
            logger.warn { "GroupApi::addParentsToGroup cannot add own id as parent" }
            // one of the parents to be added is the group itself
            throw ConflictResponse()
        } else if (toAdd.any { group.isParentGroup(it, credentialProvider) }) {
            logger.warn { "GroupApi::addParentsToGroup cannot add already parents that are already member of parent group" }
            /* one of the parents to be added is already
             * a direct parent or a parent somewhere up in the hierarchy */
            throw ConflictResponse()
        } else if (toAdd.any { group.isGroupMemberRecursive(it, credentialProvider) }) {
            logger.warn { "GroupApi::addParentsToGroup cannot add parent, it would be a recursive group member" }
            /* one of the parents to be added is already
             * a direct child or a parent somewhere up in the hierarchy */
            throw ConflictResponse()
        }

        credentialProvider.updateGroup(group.copy(parentGroups = group.parentGroups + toAdd))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun addChildrenToGroup(ctx: Context) {
        val (user, group, toAdd) = prepareAddRelativeGroups(ctx)

        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        } else if (toAdd.any { group.id == it }) {
            logger.warn { "GroupApi::addChildrenToGroup cannot add own id as child" }
            // one of the children to be added is the group itself
            throw ConflictResponse()
        } else if (toAdd.any { group.isParentGroup(it, credentialProvider) }) {
            logger.warn { "GroupApi::addChildrenToGroup cannot add child member that is already member of its parent group" }
            /* one of the children to be added is already
             * a direct parent or a parent somewhere up in the hierarchy */
            throw ConflictResponse()
        } else if (toAdd.any { group.isGroupMemberRecursive(it, credentialProvider) }) {
            logger.warn { "GroupApi::addChildrenToGroup cannot add child member, it would be a recursive group member" }
            /* one of the children to be added is already
             * a direct child or a parent somewhere up in the hierarchy */
            throw ConflictResponse()
        }

        credentialProvider.updateGroup(group.copy(groupMembers = group.groupMembers + toAdd))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun prepareAddRelativeGroups(ctx: Context): Triple<User, Group, Set<String>> {
        val session = ctx.session()
        val user = session.user
        val groupId = ctx.pathParam("id")
        val toAdd = ctx.body<Set<String>>()
        val group = credentialProvider.getGroup(groupId) ?: throw NotFoundResponse("Group $groupId not found")

        return Triple(user, group, toAdd)
    }

    private fun removeParentFromGroup(ctx: Context) {
        val (user, group, removeId) = prepareRemoveRelativeGroup(ctx)

        if (!group.parentGroups.contains(removeId)) {
            throw NotFoundResponse()
        } else if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        }

        credentialProvider.updateGroup(
            group.copy(
                parentGroups = group.parentGroups - removeId,
                ownedGroups = group.ownedGroups - removeId
            )
        )
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun removeChildFromGroup(ctx: Context) {
        val (user, group, removeId) = prepareRemoveRelativeGroup(ctx)

        if (!group.groupMembers.contains(removeId)) {
            throw NotFoundResponse()
        }
        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        }

        credentialProvider.updateGroup(
            group.copy(
                groupMembers = group.groupMembers - removeId,
                ownerGroups = group.ownerGroups - removeId
            )
        )
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun prepareRemoveRelativeGroup(ctx: Context): Triple<User, Group, String> {
        val session = ctx.session()
        val user = session.user
        val groupId = ctx.pathParam("id")
        val group = credentialProvider.getGroup(groupId) ?: throw NotFoundResponse("Group $groupId not found")
        val removeId = ctx.pathParam("memberId")

        return Triple(user, group, removeId)
    }

    private fun addOwnerGroupsToGroup(ctx: Context) {
        val (user, group, toAdd) = prepareAddRelativeGroups(ctx)

        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        } else if (toAdd.any { group.id == it }) {
            // one of the owner groups to be added is the group itself
            logger.warn { "GroupApi::addOwnerGroupsToGroup cannot add own id as owner" }
            throw ConflictResponse()
        } else if (toAdd.any { group.isOwnerGroup(it, credentialProvider) }) {
            logger.warn { "GroupApi::addOwnerGroupsToGroup cannot add member of already existing owner group" }
            /* one of the owner groups to be added is already a direct owner group
             * or an indirect owner group somewhere up in the hierarchy */
            throw ConflictResponse()
        } else if (toAdd.any { group.isOwnedGroupRecursive(it, credentialProvider) }) {
            logger.warn { "GroupApi::addOwnerGroupsToGroup cannot add owner that would be a recursive owner" }
            /* one of the owner groups to be added is already a directly owned group
             * or an indirectly owned group somewhere down in the hierarchy */
            throw ConflictResponse()
        }

        credentialProvider.updateGroup(group.copy(ownerGroups = group.ownerGroups + toAdd))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun addOwnedGroupsToGroup(ctx: Context) {
        val (user, group, toAdd) = prepareAddRelativeGroups(ctx)

        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        } else if (toAdd.any { group.id == it }) {
            // one of the groups to be owned by this group is the group itself
            logger.warn { "GroupApi::addOwnedGroupsToGroup cannot add own id as owned id" }
            throw ConflictResponse()
        } else if (toAdd.any { group.isOwnerGroup(it, credentialProvider) }) {
            /* one of the groups to be owned by this group is already a direct owner group
             * or an indirect owner group somewhere up in the hierarchy */
            logger.warn { "GroupApi::addOwnedGroupsToGroup cannot add, because it is already member of owner group" }
            throw ConflictResponse()
        } else if (toAdd.any { group.isOwnedGroupRecursive(it, credentialProvider) }) {
            /* one of the groups to be owned by this group is already a directly owned group
             * or an indirectly owned group somewhere down in the hierarchy */
            logger.warn { "GroupApi::addOwnedGroupsToGroup cannot add, it would be recursive member of owned group" }
            throw ConflictResponse()
        }

        credentialProvider.updateGroup(group.copy(ownedGroups = group.ownedGroups + toAdd))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun removeOwnerGroupFromGroup(ctx: Context) {
        val (user, group, removeId) = prepareRemoveRelativeGroup(ctx)

        if (!group.ownerGroups.contains(removeId)) {
            throw NotFoundResponse()
        } else if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        }

        credentialProvider.updateGroup(group.copy(ownerGroups = group.ownerGroups - removeId))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }

    private fun removeOwnedGroupFromGroup(ctx: Context) {
        val (user, group, removeId) = prepareRemoveRelativeGroup(ctx)

        if (!group.ownedGroups.contains(removeId)) {
            throw NotFoundResponse()
        }
        if (!group.isAdminOrOwnerOfDnParent(user, credentialProvider)) {
            throw ForbiddenResponse()
        }

        credentialProvider.updateGroup(group.copy(ownedGroups = group.ownedGroups - removeId))
        ctx.json(credentialProvider.getGroup(group.id)!!)
    }
}
