/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api

import com.shopify.promises.Promise
import dev.maximilian.feather.Permission
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.core.security.SecurityUtil.roles
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.InternalServerErrorResponse
import io.javalin.http.NotFoundResponse

private data class OpenProjectWorkpackeAnswer(
    val userId: Int,
    val workPackageStats: Map<String, Int>
)

class OpenProjectApi(app: Javalin, private val openProjectService: OpenProjectService) {
    init {
        app.routes {
            path("bindings/openproject") {
                get("/:userId/work_packages", ::handleGetWorkPackages)
                post("/sync", ::handleSync, roles(Permission.ADMIN))
            }
        }
    }

    private fun handleGetWorkPackages(ctx: Context) {
        val session = ctx.session()
        val lookupUserId = ctx.pathParam("userId")

        if (!session.user.permissions.contains(Permission.ADMIN) && session.user.id != lookupUserId) throw ForbiddenResponse()

        val lookupUser = openProjectService.getOpenProjectWorkPackages(lookupUserId) ?: throw NotFoundResponse("User $lookupUserId not found")

        val statusList = lookupUser.workPackages.map { it.status.name }

        val answer = OpenProjectWorkpackeAnswer(
            userId = lookupUser.user.id,
            workPackageStats = statusList.associateWith {
                lookupUser.workPackages.filter { e ->
                    e.status.name == it
                }.size
            }
        )
        ctx.json(answer)
    }

    private fun handleSync(ctx: Context) {
        val session = ctx.session()
        if (!session.user.permissions.contains(Permission.ADMIN)) throw ForbiddenResponse()

        openProjectService.synchronizer.synchronize()
            .whenComplete { result ->
                when (result) {
                    is Promise.Result.Success -> ctx.json(result.value)
                    is Promise.Result.Error -> throw InternalServerErrorResponse()
                }
            }
    }
}
