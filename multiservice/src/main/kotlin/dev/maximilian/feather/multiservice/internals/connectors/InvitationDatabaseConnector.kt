/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.connectors

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.internals.Invitation
import dev.maximilian.feather.multiservice.jsonb
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.time.Duration
import java.time.Instant
import java.util.Locale
import java.util.UUID

internal class InvitationDatabaseConnector(private val db: Database) {
    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(
                InvitationTable
            )
        }
        deleteAllExpiredInvitations()
    }

    fun getValidInvitationsByInviter(inviter: User) = transaction(db) {
        InvitationTable.select {
            InvitationTable.inviterId eq inviter.id and (InvitationTable.used eq false) and (InvitationTable.validUntil greaterEq Instant.now())
        }.map { rowToInvitation(it, inviter) }
    }

    fun getAllValidInvitations(credentialProvider: ICredentialProvider) = transaction(db) {
        InvitationTable.select {
            InvitationTable.used eq false and (InvitationTable.validUntil greaterEq Instant.now())
        }.map {
            credentialProvider.getUser(it[InvitationTable.inviterId])
                ?.let { user ->
                    rowToInvitation(
                        it,
                        user
                    )
                }
        }
    }

    fun getAllExpiredInvitations(credentialProvider: ICredentialProvider) = transaction(db) {
        val deadline = Instant.now().minus(Duration.ofDays(30))
        InvitationTable.select {
            InvitationTable.used eq false and (InvitationTable.validUntil less deadline)
        }.map {
            credentialProvider.getUser(it[InvitationTable.inviterId])
                ?.let { user ->
                    rowToInvitation(
                        it,
                        user
                    )
                }
        }
    }

    fun getInvitationByMail(credentialProvider: ICredentialProvider, mail: String): Invitation? = transaction(db) {
        InvitationTable.select {
            InvitationTable.mail eq mail.lowercase(Locale.getDefault()) and (InvitationTable.used eq false) and (InvitationTable.validUntil greaterEq Instant.now())
        }.firstOrNull()?.let {
            credentialProvider.getUser(it[InvitationTable.inviterId])
                ?.let { user ->
                    rowToInvitation(
                        it,
                        user
                    )
                }
        }
    }

    fun getInvitation(uuid: UUID, credentialProvider: ICredentialProvider, oldOrUsed: Boolean = false): Invitation? = transaction(
        db
    ) {
        InvitationTable.select {
            InvitationTable.id eq EntityID(
                uuid,
                InvitationTable
            )
        }.firstOrNull()?.let {
            credentialProvider.getUser(it[InvitationTable.inviterId])
                ?.let { user ->
                    rowToInvitation(
                        it,
                        user
                    )
                }
        }?.takeIf { oldOrUsed || (!it.used && it.validUntil >= Instant.now()) }
    }

    fun createInvitation(invitation: Invitation, credentialProvider: ICredentialProvider): Invitation = getInvitation(
        transaction(db) {
            InvitationTable.insertAndGetId {
                it[inviterId] = invitation.inviter.id
                it[validUntil] = invitation.validUntil
                it[used] = false
                it[firstname] = invitation.invitee.firstname
                it[surname] = invitation.invitee.surname
                it[mail] = invitation.invitee.mail.lowercase(Locale.getDefault())
                it[groups] = invitation.invitee.groups
                it[permissions] = invitation.invitee.permissions.map { p -> p.toString() }.toSet()
            }.value
        },
        credentialProvider
    )!!

    fun updateValidUntil(invitation: Invitation, newValidTo: Instant) {
        transaction {
            InvitationTable.update({ InvitationTable.id eq invitation.id }) {
                it[validUntil] = newValidTo
            }
        }
    }

    fun finalize(invitation: Invitation) {
        transaction {
            InvitationTable.update({ InvitationTable.id eq invitation.id }) {
                it[used] = true
            }
        }
    }

    fun deleteInvitation(invitation: Invitation) {
        transaction(db) { InvitationTable.deleteWhere { InvitationTable.id eq invitation.id } }
    }

    fun deleteAllExpiredInvitations() {
        /* Would not get invitations from deleted inviters:
        getAllExpiredInvitations.forEach{
            deleteInvitation(it)
        }*/
        val deadline = Instant.now().minus(Duration.ofDays(30))
        transaction(db) { InvitationTable.deleteWhere { InvitationTable.used eq false and (InvitationTable.validUntil less deadline) } }
    }

    private fun rowToInvitation(it: ResultRow, inviter: User): Invitation =
        Invitation(
            inviter = inviter,
            validUntil = it[InvitationTable.validUntil],
            used = it[InvitationTable.used],
            id = it[InvitationTable.id].value,
            invitee = User(
                id = "",
                surname = it[InvitationTable.surname],
                username = "",
                displayName = "",
                mail = it[InvitationTable.mail],
                ownedGroups = emptySet(),
                groups = it[InvitationTable.groups],
                permissions = it[InvitationTable.permissions].map { Permission.valueOf(it) }.toSet(),
                firstname = it[InvitationTable.firstname],
                registeredSince = Instant.ofEpochSecond(0)
            )
        )

    object InvitationTable : UUIDTable("invites") {
        private val objectMapper = jacksonObjectMapper()

        val inviterId: Column<String> = varchar("inviterId", 255)
        val validUntil: Column<Instant> = timestamp("validUntil")
        val used: Column<Boolean> = bool("used")
        val firstname: Column<String> = varchar("firstname", 30)
        val surname: Column<String> = varchar("surname", 30)
        val mail: Column<String> = varchar("mail", 100)
        val groups: Column<Set<String>> = jsonb("groups", objectMapper)
        val permissions: Column<Set<String>> = jsonb("permissions", objectMapper)
    }
}
