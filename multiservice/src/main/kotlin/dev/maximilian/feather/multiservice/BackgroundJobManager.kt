/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.Permission
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mu.KotlinLogging
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.params.SetParams
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import kotlin.concurrent.timer

data class BackgroundJob(
    val id: UUID,
    val permission: Permission?, // can be null to allow anonymous access
    val created: Instant,
    val status: String,
    val completed: Instant? = null,
    val exception: String? = null
)

class BackgroundJobManager(private val pool: JedisPool) {
    /* Default cache expiration time: 30 minutes.
     * Much longer than the cleanup time of 10 minutes,
     * because we don't know how long the job will run. */
    private val setParams = SetParams().ex(30 * 60)
    private val objectMapper = jacksonObjectMapper().registerModule(JavaTimeModule())

    private val logger = KotlinLogging.logger {}

    init {
        // cleanup every 10 minutes
        timer(daemon = true, period = 10 * 60 * 1000L, initialDelay = 10 * 60 * 1000L) { cleanupJobs() }
    }

    private fun cleanupJobs() {
        val jobsByStatus = getJobs()
            .groupBy { it.completed != null && it.completed <= Instant.now().minus(10, ChronoUnit.MINUTES) }

        redisTransaction { jedis ->
            jobsByStatus[true] ?.let { list ->
                list.forEach {
                    jedis.del("feather.background_job.${it.id}")
                    jedis.del("feather.background_job_results.${it.id}")
                }
            }
            // remaining jobs
            jedis.del("feather.background_jobs")
            jobsByStatus[false] ?.let { list ->
                val idList = list.map { it.id.toString() }
                jedis.sadd("feather.background_jobs", *idList.toTypedArray())
            }
        }
    }

    private suspend fun <Targ, Tresult> _runJob(id: UUID, job: suspend (UUID, Targ) -> Tresult, argument: Targ) {
        var result: Tresult? = null
        var exceptionText: String? = null

        try {
            result = job(id, argument)
        } catch (e: Exception) {
            logger.error(e) { "Background job $id failed with exception" }
            exceptionText = e.toString()
        }
        getJobStatus(id) ?.let { backgroundJob ->
            completeJob(backgroundJob, result, exceptionText)
        }
    }

    fun <Targ, Tresult> runBackgroundJob(
        job: suspend (UUID, Targ) -> Tresult,
        argument: Targ,
        permission: Permission? = Permission.ADMIN
    ): BackgroundJob {
        val newJob = addJob(permission)
        GlobalScope.launch { _runJob(newJob.id, job, argument) }
        logger.info { "Background job ${newJob.id} started" }
        return newJob
    }

    private fun addJob(permission: Permission?): BackgroundJob {
        val newJob = BackgroundJob(
            UUID.randomUUID(),
            permission,
            Instant.now(), "started"
        )
        redisTransaction { jedis ->
            jedis["feather.background_job.${newJob.id}"] = newJob
            jedis.sadd("feather.background_jobs", newJob.id.toString())
        }
        return newJob
    }

    private fun <T> completeJob(job: BackgroundJob, results: T?, exceptionText: String?): BackgroundJob {
        val completedJob = job.copy(
            status = "completed",
            completed = Instant.now(),
            exception = exceptionText
        )
        redisTransaction { jedis ->
            jedis["feather.background_job.${job.id}"] = completedJob
            if (results != null) {
                jedis["feather.background_job_results.${job.id}"] = results
            }
        }
        return completedJob
    }

    fun getJobStatus(uuid: UUID): BackgroundJob? {
        return _getJob(uuid.toString())
    }

    fun setJobStatus(uuid: UUID, status: String): BackgroundJob? {
        return _getJob(uuid.toString())?.let {
            val modifiedJob = it.copy(status = status)
            redisTransaction { jedis ->
                jedis["feather.background_job.${modifiedJob.id}"] = modifiedJob
            }
            modifiedJob
        }
    }

    inline fun <reified T> getJobResults(job: BackgroundJob): T? =
        job.completed?.let { extractJobResults(job.id, T::class.java) }

    fun <T> extractJobResults(id: UUID, clazz: Class<T>): T? =
        _getJobResults(id.toString())?.let { objectMapper.readValue(it, clazz) }

    fun getJobResultsAsString(job: BackgroundJob): String? {
        if (job.completed == null) {
            return null
        } else {
            // gets the string of the json-encoded results
            return _getJobResults(job.id.toString())
        }
    }

    fun getJobs(): Set<BackgroundJob> = redisTransaction {
        it.smembers("feather.background_jobs").mapNotNull { jobId -> it.getObject<BackgroundJob>("feather.background_job.$jobId") }
    }.toSet()

    private fun _getJob(uuid: String): BackgroundJob? = redisTransaction { it.getObject<BackgroundJob>("feather.background_job.$uuid") }

    // gets the string of the json-encoded results
    private fun _getJobResults(uuid: String): String? = redisTransaction { it["feather.background_job_results.$uuid"] }

    private fun <T> redisTransaction(block: (Jedis) -> T): T = pool.resource.use(block)

    private operator fun <T> Jedis.set(key: String, value: T): String =
        this.set(key, objectMapper.writeValueAsString(value), setParams)

    private fun <T> Jedis.get(key: String, clazz: Class<T>): T? = this[key]?.let { objectMapper.readValue(it, clazz) }

    private inline fun <reified T> Jedis.getObject(key: String): T? = get(key, T::class.java)
}
