/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.plausibility

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.IControllableService
import dev.maximilian.feather.multiservice.StringTools
import dev.maximilian.feather.multiservice.internals.AsyncMailSender
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.multiservice.settings.CheckUserEvent
import mu.KLogging
import java.util.UUID
import javax.mail.internet.AddressException

internal enum class AutoRepair {
    JUST_CHECK, AUTOREPAIR_ALL, AUTOREPAIR_SELECTED
}

internal data class UserPlausibilityConfig(
    val checkOpenProject: Boolean,
    val checkNextcloud: Boolean,
    val allowMissingFromNextcloud: Boolean?,
    val autoRepair: AutoRepair = AutoRepair.JUST_CHECK,
    val autoRepairUserIds: Set<String>? = null
)

internal data class UserPlausibilityEntry(
    val user: User,
    val generalStatus: String,
    val openProjectStatus: String,
    val nextcloudStatus: String
)

internal data class UserPlausibilityResults(
    val entries: List<UserPlausibilityEntry>,
    val overallStatus: String
)

internal class UserPlausibility(
    private val services: List<IControllableService>,
    private val backgroundJobManager: BackgroundJobManager,
    private val credentialProvider: ICredentialProvider,
    private val externalCheck: CheckUserEvent
) {
    companion object : KLogging()

    private suspend fun checkGeneralStatus(
        user: User,
        allUsers: Collection<User>,
        autorepair: Boolean
    ): String {
        val errorMessages = mutableListOf<String>()

        if (user.firstname.isEmpty()) errorMessages += "Ein Vorname ist benötigt"
        if (user.firstname.length !in (2..40)) errorMessages += "Ein Vorname benötigt mindestens 2 und maximal 40 Zeichen"
        if (user.surname.isEmpty()) errorMessages += "Ein Nachname ist benötigt"
        if (user.surname.length !in (2..40)) errorMessages += "Ein Nachname benötigt mindestens 2 und maximal 40 Zeichen"
        if (user.mail.isEmpty()) errorMessages += "Eine E-Mail ist benötigt"
        if (user.mail.length !in (5..100)) errorMessages += "Eine E-Mail benötigt mindestens 5 und maximal 100 Zeichen"
        try {
            AsyncMailSender.validateMail(user.mail, "${user.firstname} ${user.surname}")
        } catch (e: AddressException) {
            val reason = e.toString()
            errorMessages += "Die angegebene E-Mail-Adresse ist nicht RFC822 konform (Grund: '$reason')"
        }
        if (user.username.isEmpty()) errorMessages += "Ein Anzeigename ist benötigt"
        if (user.username.length !in (3..40)) errorMessages += "Ein Anzeigename benötigt mindestens 3 und maximal 40 Zeichen"
        if (!(user.username matches StringTools.userDisplayNameRegex)) errorMessages += "Ein Anzeigename darf nur Kleinbuchstaben und Zahlen enthalten. Darf nicht mit einer Zahl starten."
        if (user.displayName == null) errorMessages += "Der Name darf nicht null sein."
        else if (user.displayName!!.isEmpty()) errorMessages += "Ein voller Name ist benötigt"
        if (user.displayName != "${user.firstname} ${user.surname}") errorMessages += "Der volle Name entspricht nicht dem Vor- und Nachnamen"

        if (allUsers.filter { it.mail.equals(user.mail, true) }.count() > 1) errorMessages += "Duplikate (Mail-Adresse) gefunden"

        externalCheck.check(user, autorepair)?.let { errorMessages.add(it) }

        return if (errorMessages.isEmpty()) { "OK" } else { errorMessages.joinToString(". ") }
    }

    suspend fun checkUserPlausibility(
        jobId: UUID,
        config: UserPlausibilityConfig
    ): UserPlausibilityResults {
        if (config.autoRepair == AutoRepair.AUTOREPAIR_SELECTED &&
            config.autoRepairUserIds == null
        )
            throw IllegalArgumentException("AUTOREPAIR_SELECTED needs a set of user IDs")

        backgroundJobManager.setJobStatus(jobId, "Fetching users")

        val allUsers = credentialProvider.getUsers()

        val entries = mutableListOf<UserPlausibilityEntry>()

        val statusHelper = BackgroundJobStatusHelper(jobId, "Processing users", allUsers.size, backgroundJobManager)

        val allowMissingFromNextcloud = config.allowMissingFromNextcloud ?: true

        var currentEntry = 0
        var allOk = true

        val openProjectService = services.firstOrNull() { it is OpenProjectService }
        val nextcloudService = services.firstOrNull() { it is NextcloudService }

        allUsers.forEach { user ->
            val autoRepair = (config.autoRepair == AutoRepair.AUTOREPAIR_ALL) ||
                (
                    config.autoRepair == AutoRepair.AUTOREPAIR_SELECTED &&
                        config.autoRepairUserIds != null &&
                        config.autoRepairUserIds.contains(user.id)
                    )

            val generalStatus = checkGeneralStatus(user, allUsers, autoRepair)
            if (generalStatus != "OK") { allOk = false }

            val openProjectStatus = when (config.checkOpenProject) {
                true -> openProjectService?.checkUser(user, autoRepair) ?: "Not available"
                else -> "Not tested"
            }
            logger.info { "UserPlausibility::checkOpenProjectUser for ${user.id}: $openProjectStatus" }
            if (config.checkOpenProject && openProjectStatus != "OK") { allOk = false }

            val nextcloudStatus = when (config.checkNextcloud) {
                true -> nextcloudService?.checkUser(user, autoRepair) ?: "Not available"
                else -> "Not tested"
            }
            logger.info { "UserPlausibility::checkNextcloudUser for ${user.id}: $nextcloudStatus" }
            if (config.checkNextcloud) {
                if (allowMissingFromNextcloud) {
                    if (nextcloudStatus != "OK" && nextcloudStatus != "Not found") { allOk = false }
                } else {
                    if (nextcloudStatus != "OK") { allOk = false }
                }
            }

            entries.add(
                UserPlausibilityEntry(
                    user,
                    generalStatus, openProjectStatus, nextcloudStatus
                )
            )

            currentEntry = currentEntry + 1
            statusHelper.updateStatus(currentEntry)
        }

        return UserPlausibilityResults(
            entries,
            when (allOk) {
                true -> "OK"
                else -> "Not OK"
            }
        )
    }
}
