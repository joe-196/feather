/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.mockserver

import com.shopify.promises.Promise
import dev.maximilian.feather.Group
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.openshift.CommandOutput

class GroupSyncMock : GroupSynchronizationEvent {
    var wasTriggered = false

    var createCalls = mutableListOf<CreateCall>()

    override fun synchronize(): Promise<CommandOutput, RuntimeException> {
        wasTriggered = true
        return Promise.ofSuccess(CommandOutput(false, listOf("Done.")))
    }

    data class CreateCall(
        val openproject: IOpenProject,
        val createdLdapGroups: List<Group>,
        val newOpenProjectGroups: List<OpenProjectGroup>
    )

    override suspend fun createOPSync(
        openproject: IOpenProject,
        createdLdapGroups: List<Group>,
        newOpenProjectGroups: List<OpenProjectGroup>
    ) {
        createCalls.add(CreateCall(openproject, createdLdapGroups, newOpenProjectGroups))
    }

    fun reset() {
        wasTriggered = false
        createCalls.clear()
    }
}
