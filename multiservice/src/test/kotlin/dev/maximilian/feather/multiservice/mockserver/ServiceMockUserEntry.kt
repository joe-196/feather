package dev.maximilian.feather.multiservice.mockserver

import dev.maximilian.feather.User

data class ServiceMockUserEntry(
    var user: User,
    var active: Boolean
)
