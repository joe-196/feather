/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.multiservice.api.internal.ChangeMailAddressRequest
import dev.maximilian.feather.multiservice.settings.TestCredentialProvider
import dev.maximilian.feather.multiservice.settings.TestUser
import kong.unirest.HttpResponse
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class ChangeMailAddressTest {
    private val credentials = TestCredentialProvider.credentialProvider

    @Test
    fun `POST change mail address leads to CREATED (201)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executeChangeMail(
            apiUtilities,
            TestUser.standardLogin.password,
            "alabama@web.de",
            true
        )
        assertEquals(HttpStatus.CREATED_201, result.status)
    }
    @Test
    fun `POST change mail address without verified leads to BAD REQUEST (400)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executeChangeMail(
            apiUtilities,
            TestUser.standardLogin.password,
            "alabama@web.de",
            false
        )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST change mail address with wrong password leads to FORBIDDEN (403)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executeChangeMail(
            apiUtilities,
            TestUser.standardLogin.password + "WRONG",
            "alabama@web.de",
            true
        )
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `POST change mail address without @ in email leads to BAD REQUEST (400)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executeChangeMail(
            apiUtilities,
            TestUser.standardLogin.password,
            "noAtSign",
            true
        )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST change mail address with AE in email leads to BAD REQUEST (400)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executeChangeMail(
            apiUtilities,
            TestUser.standardLogin.password,
            "ä@gmx.de",
            true
        )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST change mail address without suffix in email leads to BAD REQUEST (400)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executeChangeMail(
            apiUtilities,
            TestUser.standardLogin.password,
            "asdcas@gmx",
            true
        )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    private fun executeChangeMail(apiUtilities: ApiTestUtilities, password: String, newMail: String, verified: Boolean): HttpResponse<Any> {
        val scenario = apiUtilities.startWithDummyService(false)
        val changeMailRequest = ChangeMailAddressRequest(
            password,
            newMail,
            verified
        )
        val id = credentials.getUserByUsername(TestUser.standardUser.username)!!.id
        return apiUtilities.restConnection.post("${scenario.basePath}/change_mail/step1/$id").body(changeMailRequest).asEmpty()
    }
}
