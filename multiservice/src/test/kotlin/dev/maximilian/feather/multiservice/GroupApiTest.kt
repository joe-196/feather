/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.Group
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.settings.TestCredentialProvider
import dev.maximilian.feather.multiservice.settings.TestUser
import kong.unirest.GenericType
import kong.unirest.HttpRequest
import kong.unirest.HttpResponse
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class GroupApiTest {
    private val credentials = TestCredentialProvider.credentialProvider

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> =
        this.asObject(object : GenericType<B>() {})

    @Test
    fun `Get groups delivers OK (200)`() {
        val apiTestUtilities = ApiTestUtilities()
        apiTestUtilities.startWithDummyService(false)
        apiTestUtilities.loginWithStandardUser()

        val ret = apiTestUtilities.restConnection.get("${apiTestUtilities.scenario!!.basePath}/groups").asEmpty()
        assertEquals(HttpStatus.OK_200, ret.status)
    }

    @Test
    fun `Get groups delivers correct groups`() {
        val ret = getAllGroups(false)
        assertEquals(2, ret.body.size)
        assertNotNull(ret.body.firstOrNull() { it.name == "testgroup1" })
        assertNotNull(ret.body.firstOrNull() { it.name == "testgroup2" })
    }

    private fun getAllGroups(admin: Boolean): HttpResponse<Collection<Group>> {
        val apiTestUtilities = ApiTestUtilities()
        apiTestUtilities.startWithDummyService(admin)
        val t1 = Group("", "testgroup1", "Test Group 1", "", setOf(), setOf(), setOf(), setOf(), setOf(), setOf())
        val t2 = Group("", "testgroup2", "Test Group 2", "", setOf(), setOf(), setOf(), setOf(), setOf(), setOf())

        credentials.createGroup(t1)
        credentials.createGroup(t2)
        return apiTestUtilities.restConnection.get("${apiTestUtilities.scenario!!.basePath}/groups").asObject<Collection<Group>>()
    }

    @Test
    fun `Get testgroup delivers OK (200)`() {
        val ret = getGroups("testgroup2", false)
        assertEquals(HttpStatus.OK_200, ret.status)
    }

    @Test
    fun `Get testgroup is correct`() {
        val ret = getGroups("testgroup2", true)
        assertEquals("testgroup2", ret.body.name)
    }

    private fun getGroups(nameOfSecondGroup: String, admin: Boolean): HttpResponse<Group> {
        val apiTestUtilities = ApiTestUtilities()
        apiTestUtilities.startWithDummyService(admin)
        val t1 = Group("", "testgroup1", "Test Group 1", "", setOf(), setOf(), setOf(), setOf(), setOf(), setOf())
        val t2 = Group("", nameOfSecondGroup, "Test Group 2", "", setOf(), setOf(), setOf(), setOf(), setOf(), setOf())

        credentials.createGroup(t1)
        val secondCreatedGroup = credentials.createGroup(t2)
        return apiTestUtilities.restConnection.get("${apiTestUtilities.scenario!!.basePath}/groups/${secondCreatedGroup.id}").asObject<Group>()
    }

    @Test
    fun `Add member to group delivers correct group`() {
        val q = executeAddMemberToGroup("testgroup1", true)
        assertEquals("testgroup1", q.body.name)
    }

    @Test
    fun `Add member to group as owner delivers OK (200)`() {
        val q = executeAddMemberToGroupAsOwner("testgroup1")
        assertEquals(HttpStatus.OK_200, q.status)
    }

    @Test
    fun `Add member to group delivers OK (200)`() {
        val q = executeAddMemberToGroup("testgroup1", true)
        assertEquals(HttpStatus.OK_200, q.status)
    }

    @Test
    fun `Add member to group by standard user delivers FORBIDDEN (403)`() {
        val q = executeAddMemberToGroup("testgroup1", false)
        assertEquals(HttpStatus.FORBIDDEN_403, q.status)
    }

    private fun executeAddMemberToGroup(groupName: String, admin: Boolean): HttpResponse<Group> {
        val apiTestUtilities = ApiTestUtilities()
        apiTestUtilities.startWithDummyService(admin)
        val t1 = Group("", groupName, "Test Group 1", "", setOf(), setOf(), setOf(), setOf(), setOf(), setOf())
        val userID = credentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        val createdGroup = credentials.createGroup(t1)
        return apiTestUtilities.restConnection.put("${apiTestUtilities.scenario!!.basePath}/groups/${createdGroup.id}/member").body(setOf(userID)).asObject<Group>()
    }

    private fun executeAddMemberToGroupAsOwner(groupName: String): HttpResponse<Group> {
        val apiTestUtilities = ApiTestUtilities()
        apiTestUtilities.startWithDummyService(false)
        val userKarl = User(
            "", "KumpelKarl", "Kumpel Karl", "Karl", "Hotzenplotz", "karl@mail.de", emptySet(), Instant.now(),
            emptySet(), emptySet(), false
        )
        val karlCreated = credentials.createUser(userKarl, "123456789")
        val userID = credentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        val adminGroup = Group("", "$groupName-admin", "Admin of test Group 1", "", setOf(userID), setOf(), setOf(), setOf(), setOf(), setOf())
        val createdAdminGroup = credentials.createGroup(adminGroup)

        val memberGroup = Group("", groupName, "Test Group 1", "", setOf(), setOf(), setOf(), setOf(), setOf(createdAdminGroup.id), setOf())
        val createdMemberGroup = credentials.createGroup(memberGroup)

        return apiTestUtilities.restConnection.put("${apiTestUtilities.scenario!!.basePath}/groups/${createdMemberGroup.id}/member").body(setOf(karlCreated.id)).asObject<Group>()
    }

    @Test
    fun `Delete member from group by standard user delivers FORBIDDEN (403)`() {
        val apiTestUtilities = ApiTestUtilities()
        val q = executeDeleteMemberFromGroup(apiTestUtilities, "testgroup1", false)
        assertEquals(HttpStatus.FORBIDDEN_403, q.status)
    }

    @Test
    fun `Delete member from group as owner delivers OK (200)`() {
        val q = executeDeleteMemberFromGroupAsOwner(ApiTestUtilities(), "testgroup1")
        assertEquals(HttpStatus.OK_200, q.status)
    }

    @Test
    fun `Delete member from group delivers OK (200)`() {
        val apiTestUtilities = ApiTestUtilities()
        val q = executeDeleteMemberFromGroup(apiTestUtilities, "testgroup1", true)
        assertEquals(HttpStatus.OK_200, q.status)
    }

    @Test
    fun `Delete member from group really deletes user`() {
        val apiTestUtilities = ApiTestUtilities()
        executeDeleteMemberFromGroup(apiTestUtilities, "testgroup1", true)
        val group = credentials.getGroupByName("testgroup1")!!
        assertEquals(0, group.userMembers.size)
    }

    @Test
    fun `Delete member from group returns correct group`() {
        val apiTestUtilities = ApiTestUtilities()
        val ret = executeDeleteMemberFromGroup(apiTestUtilities, "testgroup1", true)
        assertEquals("testgroup1", ret.body.name)
    }

    private fun executeDeleteMemberFromGroup(apiTestUtilities: ApiTestUtilities, groupName: String, admin: Boolean): HttpResponse<Group> {
        apiTestUtilities.startWithDummyService(admin)
        val t1 = Group("", groupName, "Test Group 1", "", setOf(), setOf(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(t1)
        val user = credentials.getUserByUsername(TestUser.standardLogin.name)!!.copy(groups = setOf(createdGroup.id))
        credentials.updateUser(user)
        val modifiedGroup = createdGroup.copy(userMembers = setOf(user.id))
        credentials.updateGroup(modifiedGroup)

        return apiTestUtilities.restConnection.delete("${apiTestUtilities.scenario!!.basePath}/groups/${createdGroup.id}/member/${user.id}").asObject<Group>()
    }

    private fun executeDeleteMemberFromGroupAsOwner(apiTestUtilities: ApiTestUtilities, groupName: String): HttpResponse<Group> {
        apiTestUtilities.startWithDummyService(false)
        val userID = credentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        val adminGroup = Group("", "$groupName-admin", "Admin of test Group 1", "", setOf(userID), setOf(), setOf(), setOf(), setOf(), setOf())
        val createdAdminGroup = credentials.createGroup(adminGroup)

        val userKarl = User(
            "", "KumpelKarl", "Kumpel Karl", "Karl", "Hotzenplotz", "karl@mail.de", emptySet(), Instant.now(),
            emptySet(), emptySet(), false
        )
        val karlCreated = credentials.createUser(userKarl, "123456789")

        val t1 = Group("", groupName, "Test Group 1", "", setOf(karlCreated.id), setOf(), setOf(), setOf(), setOf(createdAdminGroup.id), setOf())
        val createdGroup = credentials.createGroup(t1)

        return apiTestUtilities.restConnection.delete("${apiTestUtilities.scenario!!.basePath}/groups/${createdGroup.id}/member/${karlCreated.id}").asObject<Group>()
    }
}
