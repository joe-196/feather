/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.multiservice.mockserver.GroupSyncMock
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.multiservice.settings.OpenProjectTestVariables
import dev.maximilian.feather.multiservice.settings.TestCredentialProvider
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class OpenProjectServiceTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val groupSynchronizationEvent = GroupSyncMock()
    private val credentialProvider = TestCredentialProvider.credentialProvider

    private val opService = OpenProjectService(backgroundJobManager, groupSynchronizationEvent, OpenProjectTestVariables.settings, credentialProvider, 3)
    private val generalServiceTest = ServiceTestUtilities(opService)

    @Test
    fun `OpenProject service name is OpenProject`() {
        val serviceName = opService.getServiceName()
        assertEquals("OpenProject", serviceName)
    }

    @Test
    fun `OpenProject service can create test user`() {
        val success = generalServiceTest.createSimpleUserTest()
        assertTrue(success)
    }

    /* Deactivated test; It needs an activated "User accounts deletable by admin" in User Settings menu of OpenProject;
       This cannot be checked by environment variable in test environment yet for automatic test

    @Test
    fun `delete of freshly created test user returns TRUE `() {
        val testUser = generalServiceTest.createRandomTestUser(true)

        val r = opService.deleteUser(testUser)
        assertTrue(r)
    }
    */

    @Test
    fun `delete nonexisting user returns TRUE`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val result = opService.deleteUser(testUser)
        assertTrue(result)
    }

    @Test
    fun `activateUser(FALSE) of an active test user leads to result TRUE (success)`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val success = opService.activateUser(testUser, false)
        assertEquals(true, success)
        opService.deleteUser(testUser)
    }

    @Test
    fun `activateUser(TRUE) of an inactive test user leads to result TRUE (success)`() {
        val testUser = generalServiceTest.createRandomTestUser(false)
        val success = opService.activateUser(testUser, true)
        assertEquals(true, success)
        opService.deleteUser(testUser)
    }

    @Test
    fun `activateUser(FALSE) of an inactive test user leads to result FALSE (failed)`() {
        val testUser = generalServiceTest.createRandomTestUser(false)
        val success = opService.activateUser(testUser, false)
        assertEquals(false, success)
        opService.deleteUser(testUser)
    }

    @Test
    fun `activateUser(TRUE) of an active test to result FALSE (failed)`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val success = opService.activateUser(testUser, true)
        assertEquals(false, success)
        opService.deleteUser(testUser)
    }

    @Test
    fun `activateUser(TRUE) of an not existing user leads to FALSE (failed)`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val success = opService.activateUser(testUser, true)
        assertEquals(false, success)
        opService.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an existing user leads to TRUE`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val resultString = runBlocking { opService.checkUser(testUser) }
        assertEquals("OK", resultString)
        opService.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an existing user with wrong email leads to Mail address differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val testUserWrong = testUser.copy(mail = "somethingwrong@test.de")
        val resultString = runBlocking { opService.checkUser(testUserWrong) }
        assertEquals("Mail address differs: ${testUser.mail}", resultString)
        opService.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an disabled user with wrong status leads to status differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val testUserWrong = testUser.copy(disabled = true)
        val resultString = runBlocking { opService.checkUser(testUserWrong) }
        assertEquals("User status differs: active", resultString)
        opService.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an non existing user leads to not found message`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val resultString = runBlocking { opService.checkUser(testUser) }
        assertEquals("Not found", resultString)
    }

    @Test
    fun `ChangeMail of an existing user leads to TRUE result`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val result = opService.changeMailAddress(testUser, "somethingnew${Random.nextInt(10000)}A@mail.de")
        assertTrue(result)
        opService.deleteUser(testUser)
    }

    @Test
    fun `ChangeMail and check user leads to Mailaddress differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val newMail = "somethingnew${Random.nextInt(10000)}@mail.de"
        opService.changeMailAddress(testUser, newMail)
        val resultString = runBlocking { opService.checkUser(testUser) }
        assertEquals("Mail address differs: $newMail", resultString)
        opService.deleteUser(testUser)
    }

    @Test
    fun `ChangeMail of inexistant user leads to FALSE`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val result = opService.changeMailAddress(testUser, "somethingnew${Random.nextInt(10000)}B@mail.de")
        assertFalse(result)
    }
}
