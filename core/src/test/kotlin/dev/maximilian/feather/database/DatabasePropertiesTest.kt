/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.database

import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import java.sql.DriverManager
import java.util.UUID

class DatabasePropertiesTest {
    companion object {
        private val db = Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:feather;DB_CLOSE_DELAY=-1") })
    }

    @Test
    fun `Test empty property database`() {
        assert(DatabaseProperties(db).isEmpty())
    }

    @Test
    fun `Test get, put one element`() {
        val testKey = UUID.randomUUID().toString()
        val testValue = UUID.randomUUID().toString()

        DatabaseProperties(db).apply {
            assertNull(put(testKey, testValue))
        }

        DatabaseProperties(db).apply {
            assertEquals(testValue, this[testKey])
        }
    }

    @Test
    fun `Test clear, putAll`() {
        val propertyMap = (0 until 10).associate { UUID.randomUUID().toString() to UUID.randomUUID().toString() }

        DatabaseProperties(db).apply { putAll(propertyMap) }
        assertEquals(propertyMap, DatabaseProperties(db))

        DatabaseProperties(db).clear()
        assert(DatabaseProperties(db).isEmpty())
    }

    @Test
    fun `Test key iterator`() {
        val propertyMap = (0 until 10).associate { UUID.randomUUID().toString() to UUID.randomUUID().toString() }
        DatabaseProperties(db).apply { putAll(propertyMap) }
        DatabaseProperties(db).keys.clear()
        assert(DatabaseProperties(db).isEmpty())
    }
}
