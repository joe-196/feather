/*
 *    Copyright [2021] Maximilian Hippler <hello@maximilian.dev>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.Main
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.authorization.LdapCredentialProvider
import dev.maximilian.feather.bindings.openshift.Openshift
import dev.maximilian.feather.iog.settings.IogConfig
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import dev.maximilian.feather.openshift.OpenShiftConfig
import dev.maximilian.feather.openshift.OpenshiftOpenProjectLDAPSync
import dev.maximilian.feather.util.FeatherProperties

internal class IogPluginFactory {
    companion object {
        fun create(backgroundJobManager: BackgroundJobManager, sessionOps: ISessionOperations, externalUserDeletionEvents: List<UserDeletionEvent>, externalUserCreationEvents: List<UserCreationEvent>): IogPlugin {
            val openShiftSettings = OpenShiftConfig(
                FeatherProperties.OPENSHIFT_BASE_URL,
                FeatherProperties.OPENSHIFT_USERNAME,
                FeatherProperties.OPENSHIFT_PASSWORD,
                Main.PROPERTIES
            )
            val openshift = Openshift(openShiftSettings)
            val baseDN = (Main.UNCACHED_CREDENTIAL_MANAGER as LdapCredentialProvider).ldap.baseDnGroups
            val iogConfig = IogConfig(
                baseDN,
                Main.DATABASE,
                ServiceFactory.isSupportMembershipDbEnabled(),
                Main.CREDENTIAL_MANAGER,
                OPNameConfig(Main.PROPERTIES),
                backgroundJobManager,
                FeatherProperties.NEXTCLOUD_PUBLIC_URL,
                Main.REDIS,
                sessionOps,
                OpenshiftSynchronizer(OpenshiftOpenProjectLDAPSync(openshift), baseDN) as GroupSynchronizationEvent,
                openshift.enabled,
                FeatherProperties.IOG_SUPPORT_MEMBERSHIP_HASH_ALGO
            )
            return IogPlugin(iogConfig, externalUserDeletionEvents, externalUserCreationEvents)
        }
    }
}
